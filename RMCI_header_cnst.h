//	**************************************************************************************
//	
//	Project Name: Alvara� Radio Modem Control Interface
//	
//	File Name: RMCI_header_cnst.c
//	
//	Release:	C10 - FW01 - 2.1.1
//	
//	Author:	F. Agnello
//	
//	**************************************************************************************
//	
//	� Ceravision Ltd - 2012
//	
//	*******************************************
//
//	REVISION HISTORY
//	--------------------------------------------------------------------------
//
//	* RELEASE 2.1.1 *
//
//	1. Added a difference between generation 1 and 2 of ALVARA when compiling and
//	when reading the firnware ID. ALAVARA_GEN is used to make the difference when compiling
//
//	* RELEASE 2.1.0 *
//
//	1. Added the function code RESET_DEVICE to reset the device
//
//	2. Added new error codes to make troubleshooting easier:
//	- FC_NACK = function code not recognize
//	- FC_CAST_ERROR = error in casting message
//	- RM_NACK_ERROR = Radio Modem messaging not acknowledge
//	- RM_TIMEOUT_ERROR = RadioModem messaging time out
//
//	3. The function code Get_Unid_ID have been removed because
//	meaningless
//


/*
  FUNCTION CODE DEFINITION
*/
#define BROADCAST 0x00
#define MULTICAST 0x00
//
// READ FUNCTION CODE
//
#define READ_RAM 0x00
#define READ_SLAVE_STATUS 0x05
#define READ_RADIO_MODEM_CHANNEL 0x06
#define READ_RADIO_MODEM_CHANNEL_EEPROM 0x07
#define GET_DEVICE_ID		0x0A
#define GET_FW_ID			0x0B
#define GET_UNIT_ID			0x0C
#define GET_SCENE 			0x0D
#define GET_MASTER_TIMEOUT	0x0E
#define READ_ALL_EEPROM  0x3F
#define READ_EEPROM 0x40
//
// WRITE FUNCTION CODE
//
#define WRITE_RAM 0x80
#define SET_RADIO_MODEM_CHANNEL 0x86
#define SET_RADIO_MODEM_CHANNEL_EEPROM 0x87
#define GOTO_SCENE	0x88
#define SET_DEVICE_ID 		0x8A
#define SET_FW_ID			0x8B
#define SET_UNIT_ID			0x8C
#define SET_SCENE			0x8D
#define SET_MASTER_TIMEOUT	0x8E
#define WRITE_EEPROM 0xC0
//
// SPECIAL FUCNTION
//
#define SET_COMMAND_MODE		0xC8
#define EXIT_COMMAND_MODE 		0xC9
#define RESET_DEVICE				0xCA

/*
*  EEPROM ADDRESS
*/
// from 0x00 to 0x0F
#define EEPROM_GROUP_ADD		0x00
#define EEPROM_UNIT_ADD			0x01
#define EEPROM_MASTER_TIMEOUT 	0x02
#define EEPROM_RF_CHANNEL		0x03
 // from 0x10 to 0x2F
#define EEPROM_SCENE_ADD		0x10
// from 0xE0 to 0xFF
#define EEPROM_DEVICE_ID 			0xE0

/*
 *  MESSAGE HEADER MAP
*/
#define FRAME_HEADER_LEN 4
#define FRAME_FOOTER_LEN 2
#define GROUP_ADDRESS 0
#define UNIT_ADDRESS 1
#define FUNCTION_CODE 2
#define BYTE_COUNT 3
#define DATA_START 4

/*
*  UNIT STATUS PARAMETERS
*/
// EEPROM
#define MAX_NUMBER_RESTART 26
#define NUMBER_OF_RESTART 27
#define ALARMS 24
// RAM
#define POWER_SETTING 37
#define ELECTRICAL_POWER 57


/*
 *  FUNCTION ERROR LIST
*/
#define FUNCTION_CODE_ERROR 0xFF
#define NO_ERROR				0
#define CHECKSUM_ERROR		255
#define MD_NACK_ERROR		254
#define MD_TIMEOUT_ERROR		253
#define FC_NACK				252
#define FC_CAST_ERROR		251
#define RM_NACK_ERROR		250
#define RM_TIMEOUT_ERROR		249
//////////////////////////////////
// internal error fucntion
#define NACK_ERROR			254


/*
*  SYSTEM STATUS
*/
#define IDLE 0
#define COMMAND_RECEIVED 1
#define POLL_MAGDRIVE 2
#define RELEASE_MAGDRIVE 3
#define BOOTSTRAP 4


/*
*  GENERAL
*/
// Boolean
#define TRUE 1
#define True 1
#define true 1
#define FALSE 0
#define False 0
#define false 0

// Maximum allowed number of message request without answer
#define MAX_RETRY 1

#define TRANSCEIVER 1
#define MAGDRIVE 2
#define CMD_MODE 3

#define INT_BUFFER_DIM 4
#define EXT_BUFFER_DIM 70
#define OUT_BUFFER_DIM 70
#define MAX_ID_LEN	 31				

// Rx Idle state time out: 5msec @ 4MHz
#define RX_IDLE_TIMEOUT 5

// MagDrive Out Bound Time Out: 50msec @ 4MHz
#define MD_OUTBOUND_TIMEOUT 50

// RadioModem Out Bound Time Out: 100msec @ 4MHz
#define RM_OUTBOUND_TIMEOUT 100

// RM_OUTBOUND_TIMEOUT online check out 
#if RX_IDLE_TIMEOUT >= RM_OUTBOUND_TIMEOUT
#error Online setting check out: RM_OUTBOUND_TIMEOUT must be higher then RX_IDLE_TIMEOUT
#endif

// MAGDRIVE BEACON TIMEOUT: 20sec @ 4MHz
#define MD_BEACON_TIMEOUT 20000

// MAGDRIVE BEACON TIMEOUT AT POWER UP: 2sec @ 4MHz
//#define MD_SYNC_TIMEOUT 8000

//COMMAND MODE SYNC TIME OUT: 2 sec @4MHz
#define CMD_MODE_TIMEOUT 4000

//BOOTSTRAP TIME OUT: 2 sec @4MHz
#define BOOTSTRAP_TIMEOUT 2000

 // Timer0 counter value: 1 msec @ 4MHz: (256 - TMRO_START_VALUE)*250usec
#define TMRO_START_VALUE 252

// Master inbound message time out: 30 sec @ 4MHz
#define MIN_MASTER_TIMEOUT 1
#define MAX_MASTER_TIMEOUT 90
#define MASTER_TIMEOUT_RELOAD 10000

// Power Supply EEPROM length
#define PS_EEPROM_LEN 64

// Lighting scenes number
#define LIGHTING_SCENE_NUMBER 32

// Power Supply Status Length
#define PS_STATUS_LEN 6

// eeprom empty value
#define VALUE_EMPTY 255

// UNIT ID LENGTH
#define UNIT_ID_LEN 2

// MDDRIVE INBOUND MESSAGE LENGHTS
#define MD_OUTBOUND_MESSAGE_LEN 3

// DEFINE MACRO
#define DISABLE_TRANSCEIVER	PORTBbits.RB4=1;
#define ENABLE_TRANSCEIVER	PORTBbits.RB4=0;
#define DISABLE_MAGDRIVE		PORTBbits.RB3=1;
#define ENABLE_MAGDRIVE		PORTBbits.RB3=0;

#if ALVARA_GEN == 0

	// ALVARA G1
	// Default Scene Values
	#define SC0_DEFAULT 0	//POWER OFF
	#define SC1_DEFAULT 48 //30%
	#define SC2_DEFAULT 160 //100%
	
	// Higher High Power Level
	#define HIGHER_HIGH_POWER_LEVEL 160
	
	// Max Power Level
	#define MAX_POWER 160
	
	#define ALVARA_GENDER '1'

#elif ALVARA_GEN == 1 

	// ALVARA G2
	// Default Scene Values
	#define SC0_DEFAULT 0	//POWER OFF
	#define SC1_DEFAULT 30 //30%
	#define SC2_DEFAULT 100 //100%
	
	// Higher High Power Level
	#define HIGHER_HIGH_POWER_LEVEL 100
	
	// Max Power Level
	#define MAX_POWER 100
	
	#define ALVARA_GENDER '2'

#endif