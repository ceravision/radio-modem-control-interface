//	**************************************************************************************
//	
//	Project Name: Alvara� Radio Modem Control Interface
//	
//	File Name: RMCI_header_vrbl.h
//	
//	Release:	C10 - FW01 - 2.0.0
//	
//	Author:	F. Agnello
//	
//	**************************************************************************************
//	
//	� Ceravision Ltd - 2012
//	
//	*******************************************
//	*******************************************
//	* GLOBAL VARIABLE
//	*******************************************
//	
//	*******************************************
//	Changes compaired to previous version [ 1.5.0 ]
//	--------------------------------------------------------------------------
//	1. New variable CmdMode_TimeOut_Timer replace CmdMode_TimeOut
// 	as timer for software command mode time out. CmdMode_TimeOut is now used
//	as flag indicating when the timer ticks the time out
//
//	2. Bootstrap_Timer and Bootstrap_Timer_TimeOut are used to manage
//	the one off opeartions required at the bootstrapp only. 
//


// InBound Message Buffer from Power Supply
unsigned char IntBuffer[INT_BUFFER_DIM];
unsigned char IntBufferPos=0;
unsigned char IntCharCount;

// InBound Message Buffer from RF Transceiver
unsigned char ExtBuffer[EXT_BUFFER_DIM];
unsigned char ExtBufferPos=0;

// OutBound Message Buffer
unsigned char OutModemBuffer[OUT_BUFFER_DIM];

// InBound message flag
unsigned char InBoundMessage=FALSE;
unsigned char InBoundModemMessage=0;

// System status
unsigned char SystemStatus;

// On line master status
unsigned char PowerOnReset;
unsigned char Master_OnLine;
unsigned int	 Master_Inner_TimeOut_Counter;
unsigned char Master_OffLine_TimeOut_Reload;
unsigned char Master_Outer_TimeOut_Counter;
unsigned char Release_Alvara;

// Driver buffer type
unsigned char BufferMode;


// General
unsigned char RxStatus;
unsigned char RxModemTimeOut;
unsigned char InBoundMessage_TimeOut;

unsigned char TimeOut_Timer;
unsigned int	 ScanMD_TimeOut;

unsigned char RxIdleCheck;
unsigned char ScanMagDrive; 

unsigned char FrameLength;
unsigned char DataLength;
 
unsigned char ModemChannel;  	
unsigned char NewModemChannel;

unsigned char EEProm_Value;
unsigned char EEProm_Address;

unsigned char Cmd_Mode;
unsigned char Cmd_Mode_Sw;
unsigned char Cmd_Mode_Hw;

unsigned int CmdMode_TimeOut_Timer;
unsigned char CmdMode_TimeOut;

unsigned int Bootstrap_TimeOut_Timer;
unsigned char Bootstrap_TimeOut;

unsigned char RAM_RadioModem_CH;

unsigned char Broadcasted_Inbound_Message;

// Unit adrress: GroupAddress & UnitAddress
struct UnitID
{

	unsigned char GroupAddress;
	unsigned char UnitAddress;
	
}UnitID;

// Unit Status
struct UnitStatus
{

 	unsigned char MaxRestartNumber;
 	unsigned char NumberRestart;
 	unsigned char Alarms;
 	unsigned char PowerSetting;
 	unsigned char PowerReading;
	unsigned char OnLine;
 	unsigned char ExpectedPower;

}UnitStatus;

// Scene List: [0..9]
struct SceneList
{
	
	unsigned char Scene_0;
	unsigned char Scene_1;
	unsigned char Scene_2;
	unsigned char Scene_3;
	unsigned char Scene_4;
	unsigned char Scene_5;
	unsigned char Scene_6;
	unsigned char Scene_7;
	unsigned char Scene_8;
	unsigned char Scene_9;

	unsigned char Scene_10;
	unsigned char Scene_11;
	unsigned char Scene_12;
	unsigned char Scene_13;
	unsigned char Scene_14;
	unsigned char Scene_15;
	unsigned char Scene_16;
	unsigned char Scene_17;
	unsigned char Scene_18;
	unsigned char Scene_19;

	unsigned char Scene_20;
	unsigned char Scene_21;
	unsigned char Scene_22;
	unsigned char Scene_23;
	unsigned char Scene_24;
	unsigned char Scene_25;
	unsigned char Scene_26;
	unsigned char Scene_27;
	unsigned char Scene_28;
	unsigned char Scene_29;

	unsigned char Scene_30;
	unsigned char Scene_31;
	
}SceneList;