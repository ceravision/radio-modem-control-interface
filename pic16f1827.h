#ifndef	_HTC_H_
#warning Header file pic16f1827.h included directly. Use #include <htc.h> instead.
#endif

 /* header file for the MICROCHIP PIC microcontroller
    16F1827
 */

#ifndef __PIC16F1827_H
#define __PIC16F1827_H

//
// Configuratoin mask definitions
//

// Config Register: CONFIG1
#define CONFIG1              0x8007 
// Oscillator
// LP Oscillator, Low-power crystal on RA6/OSC2/CLKO pin and RA7/OSC1/CLKI
#define FOSC_LP              0xFFF8 
// XT Oscillator, Crystal/resonator on RA6/OSC2/CLKO pin and RA7/OSC1/CLKI
#define FOSC_XT              0xFFF9 
// HS Oscillator, High speed crystal/resonator on RA6/OSC2/CLKO pin and RA7/OSC1/CLKI
#define FOSC_HS              0xFFFA 
// EXTRC Oscillator, RC on RA7/OSC1/CLKIN
#define FOSC_EXTRC           0xFFFB 
// INTOSC Oscillator, I/O function on RA7/OSC1/CLKI
#define FOSC_INTOSC          0xFFFC 
// ECL, External Clock, Low Power Mode: CLKI on RA7/OSC1/CLKI
#define FOSC_ECL             0xFFFD 
// ECM, External Clock, Medium Power Mode: CLKI on RA7/OSC1/CLKI
#define FOSC_ECM             0xFFFE 
// ECH, External Clock, High Power Mode: CLKI on RA7/OSC1/CLKI
#define FOSC_ECH             0xFFFF 
// Watchdog Timer Enable bit
// WDT enabled
#define WDTE_ON              0xFFFF 
// WDT enabled while running and disabled in Sleep
#define WDTE_NSLEEP          0xFFF7 
// WDT controlled by the SWDTEN bit in the WDTCON register
#define WDTE_SWDTEN          0xFFEF 
// WDT disabled
#define WDTE_OFF             0xFFE7 
// Power-up Timer Enable bit
// PWRT disabled
#define PWRTE_OFF            0xFFFF 
// PWRT enabled
#define PWRTE_ON             0xFFDF 
// MCLR Pin Function Select
// RE3/MCLR/VPP pin function is MCLR
#define MCLRE_ON             0xFFFF 
// RE3/MCLR/VPP pin function is digital input
#define MCLRE_OFF            0xFFBF 
// Flash Program Memory Code Protection bit
// Program memory code protection is disabled
#define CP_OFF               0xFFFF 
// Program memory code protection is enabled
#define CP_ON                0xFF7F 
// Data EE Read Protect
// Data memory code protection is disabled
#define CPD_OFF              0xFFFF 
// Data memory code protection is enabled
#define CPD_ON               0xFEFF 
// Brown-out Reset Enable bits
// Brown-out Reset enabled
#define BOREN_ON             0xFFFF 
// Brown-out Reset enabled while running and disabled in Sleep
#define BOREN_NSLEEP         0xFDFF 
// Brown-out Reset controlled by the SBOREN bit in the PCON register
#define BOREN_SBODEN         0xFBFF 
// Brown-out Reset disabled
#define BOREN_OFF            0xF9FF 
// Clock Out Enable bit
// CLKOUT function is disabled. I/O or oscillator function on RA6/CLKOUT
#define CLKOUTEN_OFF         0xFFFF 
// CLKOUT function is enabled on RA6/CLKOUT pin
#define CLKOUTEN_ON          0xF7FF 
// Internal-External Switch Over
// Internal/External Switchover mode is enabled
#define IESO_ON              0xFFFF 
// Internal/External Switchover mode is disabled
#define IESO_OFF             0xEFFF 
// Fail Clock Monitor Enable
// Fail-Safe Clock Monitor is enabled
#define FCMEN_ON             0xFFFF 
// Fail-Safe Clock Monitor is disabled
#define FCMEN_OFF            0xDFFF 


// Config Register: CONFIG2
#define CONFIG2              0x8009 
// Flash memory self-write protection bits
// Write protection off
#define WRT_OFF              0xFFFF 
// 000h to 1FFh write protected, 200h to FFFh may be modified by EECON control
#define WRT_BOOT             0xFFFE 
// 000h to 7FFh write protected, 800h to FFFh may be modified by EECON control
#define WRT_HALF             0xFFFD 
// 000h to FFFh write protected, no addresses may be modified by EECON control
#define WRT_ALL              0xFFFC 
// Voltage Regulator Capacitor Enable bit
// VCAP pin function is disabled
#define VCAPEN_OFF           0xFFFF 
// VCAP functionality is enabled on VCAP pin
#define VCAPEN_ON            0xFFEF 
// PLL Enable bit
// 4x PLL enabled
#define PLLEN_ON             0xFFFF 
// 4x PLL disabled
#define PLLEN_OFF            0xFEFF 
// Stack Overflow/Underflow Reset Enable bit
// Stack Overflow or underflow will cause a Reset
#define STVREN_ON            0xFFFF 
// Stack Overflow or underflow will not cause a Reset
#define STVREN_OFF           0xFDFF 
// Brown-out Reset Voltage selection
// Brown-out Reset Voltage (VBOR) set to 1.9 V
#define BORV_19              0xFFFF 
// Brown-out Reset Voltage (VBOR) set to 2.7 V
#define BORV_27              0xFBFF 
// Debugger Mode
// Background debugger is disabled
#define DEBUG_OFF            0xFFFF 
// Background debugger is enabled
#define DEBUG_ON             0xEFFF 
// Low Voltage Programming Enable bit
// Low voltage programming enabled
#define LVP_ON               0xFFFF 
// HV on MCLR/VPP must be used for programming
#define LVP_OFF              0xDFFF 


//
// Special function register definitions: Bank 0
//

// Register: INDF0
volatile unsigned char           INDF0               @ 0x000;
// bit and bitfield definitions

// Register: INDF1
volatile unsigned char           INDF1               @ 0x001;
// bit and bitfield definitions

// Register: PCL
volatile unsigned char           PCL                 @ 0x002;
// bit and bitfield definitions

// Register: STATUS
volatile unsigned char           STATUS              @ 0x003;
// bit and bitfield definitions
volatile bit CARRY               @ ((unsigned)&STATUS*8)+0;
volatile bit DC                  @ ((unsigned)&STATUS*8)+1;
volatile bit ZERO                @ ((unsigned)&STATUS*8)+2;
volatile bit nPD                 @ ((unsigned)&STATUS*8)+3;
volatile bit nTO                 @ ((unsigned)&STATUS*8)+4;
#ifndef _LIB_BUILD
union {
    struct {
        volatile unsigned C                   : 1;
        volatile unsigned DC                  : 1;
        volatile unsigned Z                   : 1;
        volatile unsigned nPD                 : 1;
        volatile unsigned nTO                 : 1;
        volatile unsigned                     : 2;
        volatile unsigned                     : 1;
    };
} STATUSbits @ 0x003;
#endif
// bit and bitfield definitions

// Register: FSR0L
volatile unsigned char           FSR0L               @ 0x004;
// bit and bitfield definitions

// Register: FSR0H
volatile unsigned char           FSR0H               @ 0x005;
// bit and bitfield definitions

// Register: FSR0
volatile unsigned int            FSR0                @ 0x004;
// bit and bitfield definitions

// Register: FSR1L
volatile unsigned char           FSR1L               @ 0x006;
// bit and bitfield definitions

// Register: FSR1H
volatile unsigned char           FSR1H               @ 0x007;
// bit and bitfield definitions

// Register: FSR1
volatile unsigned int            FSR1                @ 0x006;

// Register: BSR
volatile unsigned char           BSR                 @ 0x008;
// bit and bitfield definitions
volatile bit BSR0                @ ((unsigned)&BSR*8)+0;
volatile bit BSR1                @ ((unsigned)&BSR*8)+1;
volatile bit BSR2                @ ((unsigned)&BSR*8)+2;
volatile bit BSR3                @ ((unsigned)&BSR*8)+3;
volatile bit BSR4                @ ((unsigned)&BSR*8)+4;
#ifndef _LIB_BUILD
union {
    struct {
        volatile unsigned BSR0                : 1;
        volatile unsigned BSR1                : 1;
        volatile unsigned BSR2                : 1;
        volatile unsigned BSR3                : 1;
        volatile unsigned BSR4                : 1;
    };
    struct {
        volatile unsigned BSR                 : 5;
    };
} BSRbits @ 0x008;
#endif

// Register: WREG
volatile unsigned char           WREG                @ 0x009;
// bit and bitfield definitions

// Register: PCLATH
volatile unsigned char           PCLATH              @ 0x00A;
// bit and bitfield definitions
#ifndef _LIB_BUILD
union {
    struct {
        volatile unsigned PCLATH              : 7;
    };
} PCLATHbits @ 0x00A;
#endif

// Register: INTCON
volatile unsigned char           INTCON              @ 0x00B;
// bit and bitfield definitions
volatile bit IOCIF               @ ((unsigned)&INTCON*8)+0;
volatile bit INTF                @ ((unsigned)&INTCON*8)+1;
volatile bit TMR0IF              @ ((unsigned)&INTCON*8)+2;
volatile bit IOCIE               @ ((unsigned)&INTCON*8)+3;
volatile bit INTE                @ ((unsigned)&INTCON*8)+4;
volatile bit TMR0IE              @ ((unsigned)&INTCON*8)+5;
volatile bit PEIE                @ ((unsigned)&INTCON*8)+6;
volatile bit GIE                 @ ((unsigned)&INTCON*8)+7;
#ifndef _LIB_BUILD
union {
    struct {
        volatile unsigned IOCIF               : 1;
        volatile unsigned INTF                : 1;
        volatile unsigned TMR0IF              : 1;
        volatile unsigned IOCIE               : 1;
        volatile unsigned INTE                : 1;
        volatile unsigned TMR0IE              : 1;
        volatile unsigned PEIE                : 1;
        volatile unsigned GIE                 : 1;
    };
} INTCONbits @ 0x00B;
#endif

// Register: PORTA
volatile unsigned char           PORTA               @ 0x00C;
// bit and bitfield definitions
volatile bit RA0                 @ ((unsigned)&PORTA*8)+0;
volatile bit RA1                 @ ((unsigned)&PORTA*8)+1;
volatile bit RA2                 @ ((unsigned)&PORTA*8)+2;
volatile bit RA3                 @ ((unsigned)&PORTA*8)+3;
volatile bit RA4                 @ ((unsigned)&PORTA*8)+4;
volatile bit RA5                 @ ((unsigned)&PORTA*8)+5;
volatile bit RA6                 @ ((unsigned)&PORTA*8)+6;
volatile bit RA7                 @ ((unsigned)&PORTA*8)+7;
#ifndef _LIB_BUILD
union {
    struct {
        volatile unsigned RA0                 : 1;
        volatile unsigned RA1                 : 1;
        volatile unsigned RA2                 : 1;
        volatile unsigned RA3                 : 1;
        volatile unsigned RA4                 : 1;
        volatile unsigned RA5                 : 1;
        volatile unsigned RA6                 : 1;
        volatile unsigned RA7                 : 1;
    };
} PORTAbits @ 0x00C;
#endif

// Register: PORTB
volatile unsigned char           PORTB               @ 0x00D;
// bit and bitfield definitions
volatile bit RB0                 @ ((unsigned)&PORTB*8)+0;
volatile bit RB1                 @ ((unsigned)&PORTB*8)+1;
volatile bit RB2                 @ ((unsigned)&PORTB*8)+2;
volatile bit RB3                 @ ((unsigned)&PORTB*8)+3;
volatile bit RB4                 @ ((unsigned)&PORTB*8)+4;
volatile bit RB5                 @ ((unsigned)&PORTB*8)+5;
volatile bit RB6                 @ ((unsigned)&PORTB*8)+6;
volatile bit RB7                 @ ((unsigned)&PORTB*8)+7;
#ifndef _LIB_BUILD
union {
    struct {
        volatile unsigned RB0                 : 1;
        volatile unsigned RB1                 : 1;
        volatile unsigned RB2                 : 1;
        volatile unsigned RB3                 : 1;
        volatile unsigned RB4                 : 1;
        volatile unsigned RB5                 : 1;
        volatile unsigned RB6                 : 1;
        volatile unsigned RB7                 : 1;
    };
} PORTBbits @ 0x00D;
#endif

// Register: PIR1
volatile unsigned char           PIR1                @ 0x011;
// bit and bitfield definitions
// TMR1 Overflow Interrupt Flag bit
volatile bit TMR1IF              @ ((unsigned)&PIR1*8)+0;
// TMR2 to PR2 Match Interrupt Flag bit
volatile bit TMR2IF              @ ((unsigned)&PIR1*8)+1;
// CCP1 Interrupt Flag bit
volatile bit CCP1IF              @ ((unsigned)&PIR1*8)+2;
// Master Synchronous Serial Port (MSSP) Interrupt Flag bit
volatile bit SSPIF               @ ((unsigned)&PIR1*8)+3;
// EUSART Transmit Interrupt Flag bit
volatile bit TXIF                @ ((unsigned)&PIR1*8)+4;
// EUSART Receive Interrupt Flag bit
volatile bit RCIF                @ ((unsigned)&PIR1*8)+5;
// A/D Converter Interrupt Flag bit
volatile bit ADIF                @ ((unsigned)&PIR1*8)+6;
// Timer1 Gate Interrupt Flag bit
volatile bit TMR1GIF             @ ((unsigned)&PIR1*8)+7;
#ifndef _LIB_BUILD
union {
    struct {
        volatile unsigned TMR1IF              : 1;
        volatile unsigned TMR2IF              : 1;
        volatile unsigned CCP1IF              : 1;
        volatile unsigned SSPIF               : 1;
        volatile unsigned TXIF                : 1;
        volatile unsigned RCIF                : 1;
        volatile unsigned ADIF                : 1;
        volatile unsigned TMR1GIF             : 1;
    };
} PIR1bits @ 0x011;
#endif

// Register: PIR2
volatile unsigned char           PIR2                @ 0x012;
// bit and bitfield definitions
volatile bit CCP2IF              @ ((unsigned)&PIR2*8)+0;
volatile bit BCL1IF              @ ((unsigned)&PIR2*8)+3;
volatile bit EEIF                @ ((unsigned)&PIR2*8)+4;
volatile bit C1IF                @ ((unsigned)&PIR2*8)+5;
volatile bit C2IF                @ ((unsigned)&PIR2*8)+6;
volatile bit OSFIF               @ ((unsigned)&PIR2*8)+7;
#ifndef _LIB_BUILD
union {
    struct {
        volatile unsigned CCP2IF              : 1;
        volatile unsigned                     : 1;
        volatile unsigned                     : 1;
        volatile unsigned BCL1IF              : 1;
        volatile unsigned EEIF                : 1;
        volatile unsigned C1IF                : 1;
        volatile unsigned C2IF                : 1;
        volatile unsigned OSFIF               : 1;
    };
} PIR2bits @ 0x012;
#endif

// Register: PIR3
volatile unsigned char           PIR3                @ 0x013;
// bit and bitfield definitions
volatile bit TMR4IF              @ ((unsigned)&PIR3*8)+1;
volatile bit TMR6IF              @ ((unsigned)&PIR3*8)+3;
volatile bit CCP3IF              @ ((unsigned)&PIR3*8)+4;
volatile bit CCP4IF              @ ((unsigned)&PIR3*8)+5;
#ifndef _LIB_BUILD
union {
    struct {
        volatile unsigned                     : 1;
        volatile unsigned TMR4IF              : 1;
        volatile unsigned                     : 1;
        volatile unsigned TMR6IF              : 1;
        volatile unsigned CCP3IF              : 1;
        volatile unsigned CCP4IF              : 1;
        volatile unsigned                     : 1;
    };
} PIR3bits @ 0x013;
#endif

// Register: PIR4
volatile unsigned char           PIR4                @ 0x014;
// bit and bitfield definitions
volatile bit SSP2IF              @ ((unsigned)&PIR4*8)+0;
volatile bit BCL2IF              @ ((unsigned)&PIR4*8)+1;
#ifndef _LIB_BUILD
union {
    struct {
        volatile unsigned SSP2IF              : 1;
        volatile unsigned BCL2IF              : 1;
    };
} PIR4bits @ 0x014;
#endif

// Register: TMR0
volatile unsigned char           TMR0                @ 0x015;
// bit and bitfield definitions
// bit and bitfield definitions

// Register: TMR1L
volatile unsigned char           TMR1L               @ 0x016;
// bit and bitfield definitions

// Register: TMR1H
volatile unsigned char           TMR1H               @ 0x017;
// bit and bitfield definitions

// Register: TMR1
volatile unsigned int            TMR1                @ 0x016;

// Register: T1CON
volatile unsigned char           T1CON               @ 0x018;
// bit and bitfield definitions
volatile bit TMR1ON              @ ((unsigned)&T1CON*8)+0;
volatile bit nT1SYNC             @ ((unsigned)&T1CON*8)+2;
volatile bit T1OSCEN             @ ((unsigned)&T1CON*8)+3;
volatile bit T1CKPS0             @ ((unsigned)&T1CON*8)+4;
volatile bit T1CKPS1             @ ((unsigned)&T1CON*8)+5;
volatile bit TMR1CS0             @ ((unsigned)&T1CON*8)+6;
volatile bit TMR1CS1             @ ((unsigned)&T1CON*8)+7;
#ifndef _LIB_BUILD
union {
    struct {
        volatile unsigned TMR1ON              : 1;
        volatile unsigned                     : 1;
        volatile unsigned nT1SYNC             : 1;
        volatile unsigned T1OSCEN             : 1;
        volatile unsigned T1CKPS0             : 1;
        volatile unsigned T1CKPS1             : 1;
        volatile unsigned TMR1CS0             : 1;
        volatile unsigned TMR1CS1             : 1;
    };
    struct {
        volatile unsigned                     : 4;
        volatile unsigned T1CKPS              : 2;
        volatile unsigned TMR1CS              : 2;
    };
} T1CONbits @ 0x018;
#endif

// Register: T1GCON
volatile unsigned char           T1GCON              @ 0x019;
// bit and bitfield definitions
volatile bit T1GSS0              @ ((unsigned)&T1GCON*8)+0;
volatile bit T1GSS1              @ ((unsigned)&T1GCON*8)+1;
volatile bit T1GVAL              @ ((unsigned)&T1GCON*8)+2;
volatile bit T1GGO               @ ((unsigned)&T1GCON*8)+3;
volatile bit T1GSPM              @ ((unsigned)&T1GCON*8)+4;
volatile bit T1GTM               @ ((unsigned)&T1GCON*8)+5;
volatile bit T1GPOL              @ ((unsigned)&T1GCON*8)+6;
volatile bit TMR1GE              @ ((unsigned)&T1GCON*8)+7;
#ifndef _LIB_BUILD
union {
    struct {
        volatile unsigned T1GSS0              : 1;
        volatile unsigned T1GSS1              : 1;
        volatile unsigned T1GVAL              : 1;
        volatile unsigned T1GGO               : 1;
        volatile unsigned T1GSPM              : 1;
        volatile unsigned T1GTM               : 1;
        volatile unsigned T1GPOL              : 1;
        volatile unsigned TMR1GE              : 1;
    };
    struct {
        volatile unsigned T1GSS               : 2;
    };
} T1GCONbits @ 0x019;
#endif

// Register: TMR2
volatile unsigned char           TMR2                @ 0x01A;
// bit and bitfield definitions

// Register: PR2
volatile unsigned char           PR2                 @ 0x01B;
// bit and bitfield definitions

// Register: T2CON
volatile unsigned char           T2CON               @ 0x01C;
// bit and bitfield definitions
volatile bit T2CKPS0             @ ((unsigned)&T2CON*8)+0;
volatile bit T2CKPS1             @ ((unsigned)&T2CON*8)+1;
volatile bit TMR2ON              @ ((unsigned)&T2CON*8)+2;
volatile bit T2OUTPS0            @ ((unsigned)&T2CON*8)+3;
volatile bit T2OUTPS1            @ ((unsigned)&T2CON*8)+4;
volatile bit T2OUTPS2            @ ((unsigned)&T2CON*8)+5;
volatile bit T2OUTPS3            @ ((unsigned)&T2CON*8)+6;
#ifndef _LIB_BUILD
union {
    struct {
        volatile unsigned T2CKPS0             : 1;
        volatile unsigned T2CKPS1             : 1;
        volatile unsigned TMR2ON              : 1;
        volatile unsigned T2OUTPS0            : 1;
        volatile unsigned T2OUTPS1            : 1;
        volatile unsigned T2OUTPS2            : 1;
        volatile unsigned T2OUTPS3            : 1;
    };
    struct {
        volatile unsigned T2CKPS              : 2;
        volatile unsigned                     : 1;
        volatile unsigned T2OUTPS             : 4;
    };
} T2CONbits @ 0x01C;
#endif

// Register: CPSCON0
volatile unsigned char           CPSCON0             @ 0x01E;
// bit and bitfield definitions
volatile bit T0XCS               @ ((unsigned)&CPSCON0*8)+0;
volatile bit CPSOUT              @ ((unsigned)&CPSCON0*8)+1;
volatile bit CPSRNG0             @ ((unsigned)&CPSCON0*8)+2;
volatile bit CPSRNG1             @ ((unsigned)&CPSCON0*8)+3;
volatile bit CPSON               @ ((unsigned)&CPSCON0*8)+7;
#ifndef _LIB_BUILD
union {
    struct {
        volatile unsigned T0XCS               : 1;
        volatile unsigned CPSOUT              : 1;
        volatile unsigned CPSRNG0             : 1;
        volatile unsigned CPSRNG1             : 1;
        volatile unsigned                     : 3;
        volatile unsigned CPSON               : 1;
    };
    struct {
        volatile unsigned                     : 2;
        volatile unsigned CPSRNG              : 2;
    };
} CPSCON0bits @ 0x01E;
#endif

// Register: CPSCON1
volatile unsigned char           CPSCON1             @ 0x01F;
// bit and bitfield definitions
volatile bit CPSCH0              @ ((unsigned)&CPSCON1*8)+0;
volatile bit CPSCH1              @ ((unsigned)&CPSCON1*8)+1;
volatile bit CPSCH2              @ ((unsigned)&CPSCON1*8)+2;
volatile bit CPSCH3              @ ((unsigned)&CPSCON1*8)+3;
#ifndef _LIB_BUILD
union {
    struct {
        volatile unsigned CPSCH0              : 1;
        volatile unsigned CPSCH1              : 1;
        volatile unsigned CPSCH2              : 1;
        volatile unsigned CPSCH3              : 1;
    };
    struct {
        volatile unsigned CPSCH               : 4;
    };
} CPSCON1bits @ 0x01F;
#endif

//
// Special function register definitions: Bank 1
//

// Register: TRISA
// PORTA Data Direction Control Register
volatile unsigned char           TRISA               @ 0x08C;
// bit and bitfield definitions
volatile bit TRISA0              @ ((unsigned)&TRISA*8)+0;
volatile bit TRISA1              @ ((unsigned)&TRISA*8)+1;
volatile bit TRISA2              @ ((unsigned)&TRISA*8)+2;
volatile bit TRISA3              @ ((unsigned)&TRISA*8)+3;
volatile bit TRISA4              @ ((unsigned)&TRISA*8)+4;
volatile bit TRISA5              @ ((unsigned)&TRISA*8)+5;
volatile bit TRISA6              @ ((unsigned)&TRISA*8)+6;
volatile bit TRISA7              @ ((unsigned)&TRISA*8)+7;
#ifndef _LIB_BUILD
union {
    struct {
        volatile unsigned TRISA0              : 1;
        volatile unsigned TRISA1              : 1;
        volatile unsigned TRISA2              : 1;
        volatile unsigned TRISA3              : 1;
        volatile unsigned TRISA4              : 1;
        volatile unsigned TRISA5              : 1;
        volatile unsigned TRISA6              : 1;
        volatile unsigned TRISA7              : 1;
    };
} TRISAbits @ 0x08C;
#endif

// Register: TRISB
// PORTB Data Direction Control Register
volatile unsigned char           TRISB               @ 0x08D;
// bit and bitfield definitions
volatile bit TRISB0              @ ((unsigned)&TRISB*8)+0;
volatile bit TRISB1              @ ((unsigned)&TRISB*8)+1;
volatile bit TRISB2              @ ((unsigned)&TRISB*8)+2;
volatile bit TRISB3              @ ((unsigned)&TRISB*8)+3;
volatile bit TRISB4              @ ((unsigned)&TRISB*8)+4;
volatile bit TRISB5              @ ((unsigned)&TRISB*8)+5;
volatile bit TRISB6              @ ((unsigned)&TRISB*8)+6;
volatile bit TRISB7              @ ((unsigned)&TRISB*8)+7;
#ifndef _LIB_BUILD
union {
    struct {
        volatile unsigned TRISB0              : 1;
        volatile unsigned TRISB1              : 1;
        volatile unsigned TRISB2              : 1;
        volatile unsigned TRISB3              : 1;
        volatile unsigned TRISB4              : 1;
        volatile unsigned TRISB5              : 1;
        volatile unsigned TRISB6              : 1;
        volatile unsigned TRISB7              : 1;
    };
} TRISBbits @ 0x08D;
#endif

// Register: PIE1
// Peripheral Interrupt Enable Register 1
volatile unsigned char           PIE1                @ 0x091;
// bit and bitfield definitions
// TMR1 Overflow Interrupt Enable bit
volatile bit TMR1IE              @ ((unsigned)&PIE1*8)+0;
// TMR2 to PR2 Match Interrupt Enable bit
volatile bit TMR2IE              @ ((unsigned)&PIE1*8)+1;
// CCP1 Interrupt Enable bit
volatile bit CCP1IE              @ ((unsigned)&PIE1*8)+2;
// Master Synchronous Serial Port (MSSP) Interrupt Enable bit
volatile bit SSP1IE              @ ((unsigned)&PIE1*8)+3;
// EUSART Transmit Interrupt Enable bit
volatile bit TXIE                @ ((unsigned)&PIE1*8)+4;
// EUSART Receive Interrupt Enable bit
volatile bit RCIE                @ ((unsigned)&PIE1*8)+5;
// A/D Converter Interrupt Enable bit
volatile bit ADIE                @ ((unsigned)&PIE1*8)+6;
// Timer1 Gate Interrupt Enable bit
volatile bit TMR1GIE             @ ((unsigned)&PIE1*8)+7;
#ifndef _LIB_BUILD
union {
    struct {
        volatile unsigned TMR1IE              : 1;
        volatile unsigned TMR2IE              : 1;
        volatile unsigned CCP1IE              : 1;
        volatile unsigned SSP1IE              : 1;
        volatile unsigned TXIE                : 1;
        volatile unsigned RCIE                : 1;
        volatile unsigned ADIE                : 1;
        volatile unsigned TMR1GIE             : 1;
    };
} PIE1bits @ 0x091;
#endif

// Register: PIE2
// Peripheral Interrupt Enable Register 2
volatile unsigned char           PIE2                @ 0x092;
// bit and bitfield definitions
// CCP2 Interrupt Enable bit
volatile bit CCP2IE              @ ((unsigned)&PIE2*8)+0;
// MSSP Bus Collision Interrupt Interrupt Enable bit
volatile bit BCL1IE              @ ((unsigned)&PIE2*8)+3;
// EEPROM Write Completion Interrupt Enable bit
volatile bit EEIE                @ ((unsigned)&PIE2*8)+4;
// Comparator C1 Interrupt Enable bit
volatile bit C1IE                @ ((unsigned)&PIE2*8)+5;
// Comparator C2 Interrupt Enable bit
volatile bit C2IE                @ ((unsigned)&PIE2*8)+6;
// CCP2 Interrupt Enable bit
volatile bit OSFIE               @ ((unsigned)&PIE2*8)+7;
#ifndef _LIB_BUILD
union {
    struct {
        volatile unsigned CCP2IE              : 1;
        volatile unsigned                     : 1;
        volatile unsigned                     : 1;
        volatile unsigned BCL1IE              : 1;
        volatile unsigned EEIE                : 1;
        volatile unsigned C1IE                : 1;
        volatile unsigned C2IE                : 1;
        volatile unsigned OSFIE               : 1;
    };
} PIE2bits @ 0x092;
#endif

// Register: PIE3
// Peripheral Interrupt Enable Register 3
volatile unsigned char           PIE3                @ 0x093;
// bit and bitfield definitions
// TMR4 to PR4 Match Interrupt Enable bit
volatile bit TMR4IE              @ ((unsigned)&PIE3*8)+1;
// TMR6 to PR6 Match Interrupt Enable bit
volatile bit TMR6IE              @ ((unsigned)&PIE3*8)+3;
// CCP3 Interrupt Enable bit
volatile bit CCP3IE              @ ((unsigned)&PIE3*8)+4;
// CCP4 Interrupt Enable bit
volatile bit CCP4IE              @ ((unsigned)&PIE3*8)+5;
#ifndef _LIB_BUILD
union {
    struct {
        volatile unsigned                     : 1;
        volatile unsigned TMR4IE              : 1;
        volatile unsigned                     : 1;
        volatile unsigned TMR6IE              : 1;
        volatile unsigned CCP3IE              : 1;
        volatile unsigned CCP4IE              : 1;
        volatile unsigned                     : 1;
    };
} PIE3bits @ 0x093;
#endif

// Register: PIE4
// Peripheral Interrupt Enable Register 4
volatile unsigned char           PIE4                @ 0x094;
// bit and bitfield definitions
volatile bit SSP2IE              @ ((unsigned)&PIE4*8)+0;
volatile bit BCL2IE              @ ((unsigned)&PIE4*8)+1;
#ifndef _LIB_BUILD
union {
    struct {
        volatile unsigned SSP2IE              : 1;
        volatile unsigned BCL2IE              : 1;
    };
} PIE4bits @ 0x094;
#endif

// Register: OPTION_REG
// Option Register
volatile unsigned char           OPTION_REG          @ 0x095;
// bit and bitfield definitions
// Prescaler Rate Select bits
volatile bit PS0                 @ ((unsigned)&OPTION_REG*8)+0;
// Prescaler Rate Select bits
volatile bit PS1                 @ ((unsigned)&OPTION_REG*8)+1;
// Prescaler Rate Select bits
volatile bit PS2                 @ ((unsigned)&OPTION_REG*8)+2;
// Prescaler Active bit
volatile bit PSA                 @ ((unsigned)&OPTION_REG*8)+3;
// TMR0 Source Edge Select bit
volatile bit T0SE                @ ((unsigned)&OPTION_REG*8)+4;
// TMR0 Clock Source Select bit
volatile bit T0CS                @ ((unsigned)&OPTION_REG*8)+5;
// Interrupt Edge Select bit
volatile bit INTEDG              @ ((unsigned)&OPTION_REG*8)+6;
// Weak Pull-up Enable bit
volatile bit nWPUEN              @ ((unsigned)&OPTION_REG*8)+7;
#ifndef _LIB_BUILD
union {
    struct {
        volatile unsigned PS0                 : 1;
        volatile unsigned PS1                 : 1;
        volatile unsigned PS2                 : 1;
        volatile unsigned PSA                 : 1;
        volatile unsigned T0SE                : 1;
        volatile unsigned T0CS                : 1;
        volatile unsigned INTEDG              : 1;
        volatile unsigned nWPUEN              : 1;
    };
    struct {
        volatile unsigned PS                  : 3;
    };
} OPTION_REGbits @ 0x095;
#endif

// Register: PCON
// Power Control Register
volatile unsigned char           PCON                @ 0x096;
// bit and bitfield definitions
// Brown-out Reset Status bit
volatile bit nBOR                @ ((unsigned)&PCON*8)+0;
// Power-on Reset Status bit
volatile bit nPOR                @ ((unsigned)&PCON*8)+1;
// RESET Instruction Flag bit
volatile bit nRI                 @ ((unsigned)&PCON*8)+2;
// MCLR Reset Flag bit
volatile bit nRMCLR              @ ((unsigned)&PCON*8)+3;
// Stack Underflow Flag bit
volatile bit STKUNF              @ ((unsigned)&PCON*8)+6;
// Stack Overflow Flag bit
volatile bit STKOVF              @ ((unsigned)&PCON*8)+7;
#ifndef _LIB_BUILD
union {
    struct {
        volatile unsigned nBOR                : 1;
        volatile unsigned nPOR                : 1;
        volatile unsigned nRI                 : 1;
        volatile unsigned nRMCLR              : 1;
        volatile unsigned                     : 1;
        volatile unsigned                     : 1;
        volatile unsigned STKUNF              : 1;
        volatile unsigned STKOVF              : 1;
    };
} PCONbits @ 0x096;
#endif

// Register: WDTCON
// Watchdog Timer Control Register
volatile unsigned char           WDTCON              @ 0x097;
// bit and bitfield definitions
// Software Enable/Disable for Watch Dog Timer bit
volatile bit SWDTEN              @ ((unsigned)&WDTCON*8)+0;
// Watchdog Timer Period Select bits
volatile bit WDTPS0              @ ((unsigned)&WDTCON*8)+1;
// Watchdog Timer Period Select bits
volatile bit WDTPS1              @ ((unsigned)&WDTCON*8)+2;
// Watchdog Timer Period Select bits
volatile bit WDTPS2              @ ((unsigned)&WDTCON*8)+3;
// Watchdog Timer Period Select bits
volatile bit WDTPS3              @ ((unsigned)&WDTCON*8)+4;
// Watchdog Timer Period Select bits
volatile bit WDTPS4              @ ((unsigned)&WDTCON*8)+5;
#ifndef _LIB_BUILD
union {
    struct {
        volatile unsigned SWDTEN              : 1;
        volatile unsigned WDTPS0              : 1;
        volatile unsigned WDTPS1              : 1;
        volatile unsigned WDTPS2              : 1;
        volatile unsigned WDTPS3              : 1;
        volatile unsigned WDTPS4              : 1;
    };
    struct {
        volatile unsigned                     : 1;
        volatile unsigned WDTPS               : 5;
    };
} WDTCONbits @ 0x097;
#endif

// Register: OSCTUNE
// Oscillator Tuning Register
volatile unsigned char           OSCTUNE             @ 0x098;
// bit and bitfield definitions
// Frequency Tuning bits
volatile bit TUN0                @ ((unsigned)&OSCTUNE*8)+0;
// Frequency Tuning bits
volatile bit TUN1                @ ((unsigned)&OSCTUNE*8)+1;
// Frequency Tuning bits
volatile bit TUN2                @ ((unsigned)&OSCTUNE*8)+2;
// Frequency Tuning bits
volatile bit TUN3                @ ((unsigned)&OSCTUNE*8)+3;
// Frequency Tuning bits
volatile bit TUN4                @ ((unsigned)&OSCTUNE*8)+4;
// Frequency Tuning bits
volatile bit TUN5                @ ((unsigned)&OSCTUNE*8)+5;
#ifndef _LIB_BUILD
union {
    struct {
        volatile unsigned TUN0                : 1;
        volatile unsigned TUN1                : 1;
        volatile unsigned TUN2                : 1;
        volatile unsigned TUN3                : 1;
        volatile unsigned TUN4                : 1;
        volatile unsigned TUN5                : 1;
    };
    struct {
        volatile unsigned TUN                 : 6;
    };
} OSCTUNEbits @ 0x098;
#endif

// Register: OSCCON
// Oscillator Control Register
volatile unsigned char           OSCCON              @ 0x099;
// bit and bitfield definitions
// System clock select bit
volatile bit SCS0                @ ((unsigned)&OSCCON*8)+0;
// System clock select bit
volatile bit SCS1                @ ((unsigned)&OSCCON*8)+1;
// Internal Oscillator Frequency Select bits
volatile bit IRCF0               @ ((unsigned)&OSCCON*8)+3;
// Internal Oscillator Frequency Select bits
volatile bit IRCF1               @ ((unsigned)&OSCCON*8)+4;
// Internal Oscillator Frequency Select bits
volatile bit IRCF2               @ ((unsigned)&OSCCON*8)+5;
// Internal Oscillator Frequency Select bits
volatile bit IRCF3               @ ((unsigned)&OSCCON*8)+6;
// Software PLL Enable bit
volatile bit SPLLEN              @ ((unsigned)&OSCCON*8)+7;
#ifndef _LIB_BUILD
union {
    struct {
        volatile unsigned SCS0                : 1;
        volatile unsigned SCS1                : 1;
        volatile unsigned                     : 1;
        volatile unsigned IRCF0               : 1;
        volatile unsigned IRCF1               : 1;
        volatile unsigned IRCF2               : 1;
        volatile unsigned IRCF3               : 1;
        volatile unsigned SPLLEN              : 1;
    };
    struct {
        volatile unsigned SCS                 : 2;
        volatile unsigned                     : 1;
        volatile unsigned IRCF                : 4;
    };
} OSCCONbits @ 0x099;
#endif

// Register: OSCSTAT
// Oscillator Status Register
volatile unsigned char           OSCSTAT             @ 0x09A;
// bit and bitfield definitions
// Low Freqency Internal Oscillator Ready bit
volatile bit HFIOFS              @ ((unsigned)&OSCSTAT*8)+0;
// Low Freqency Internal Oscillator Ready bit
volatile bit LFIOFR              @ ((unsigned)&OSCSTAT*8)+1;
// Medium Freqency Internal Oscillator Ready bit
volatile bit MFIOFR              @ ((unsigned)&OSCSTAT*8)+2;
// High Freqency Internal Oscillator Status Locked bit
volatile bit HFIOFL              @ ((unsigned)&OSCSTAT*8)+3;
// High Freqency Internal Oscillator Ready bit
volatile bit HFIOFR              @ ((unsigned)&OSCSTAT*8)+4;
// Oscillator Start-up Time-out Status bit
volatile bit OSTS                @ ((unsigned)&OSCSTAT*8)+5;
// 4x PLL Ready bit
volatile bit PLLR                @ ((unsigned)&OSCSTAT*8)+6;
// Timer1 Oscillator Ready bit
volatile bit T1OSCR              @ ((unsigned)&OSCSTAT*8)+7;
#ifndef _LIB_BUILD
union {
    struct {
        volatile unsigned HFIOFS              : 1;
        volatile unsigned LFIOFR              : 1;
        volatile unsigned MFIOFR              : 1;
        volatile unsigned HFIOFL              : 1;
        volatile unsigned HFIOFR              : 1;
        volatile unsigned OSTS                : 1;
        volatile unsigned PLLR                : 1;
        volatile unsigned T1OSCR              : 1;
    };
} OSCSTATbits @ 0x09A;
#endif
// bit and bitfield definitions

// Register: ADRESL
// A/D Result Register LSB
volatile unsigned char           ADRESL              @ 0x09B;
// bit and bitfield definitions

// Register: ADRESH
// A/D Result Register MSB
volatile unsigned char           ADRESH              @ 0x09C;
// bit and bitfield definitions

// Register: ADRES
volatile unsigned int            ADRES               @ 0x09B;

// Register: ADCON0
// Analog-to-Digital Control Register 0
volatile unsigned char           ADCON0              @ 0x09D;
// bit and bitfield definitions
// A/D Module Enable bit
volatile bit ADON                @ ((unsigned)&ADCON0*8)+0;
// A/D Conversion Status bit
volatile bit GO_nDONE            @ ((unsigned)&ADCON0*8)+1;
// Analog Channel Select bits
volatile bit CHS0                @ ((unsigned)&ADCON0*8)+2;
// Analog Channel Select bits
volatile bit CHS1                @ ((unsigned)&ADCON0*8)+3;
// Analog Channel Select bits
volatile bit CHS2                @ ((unsigned)&ADCON0*8)+4;
// Analog Channel Select bits
volatile bit CHS3                @ ((unsigned)&ADCON0*8)+5;
// Analog Channel Select bits
volatile bit CHS4                @ ((unsigned)&ADCON0*8)+6;
// A/D Conversion Status bit
volatile bit ADGO                @ ((unsigned)&ADCON0*8)+1;
#ifndef _LIB_BUILD
union {
    struct {
        volatile unsigned ADON                : 1;
        volatile unsigned GO_nDONE            : 1;
        volatile unsigned CHS0                : 1;
        volatile unsigned CHS1                : 1;
        volatile unsigned CHS2                : 1;
        volatile unsigned CHS3                : 1;
        volatile unsigned CHS4                : 1;
        volatile unsigned                     : 1;
    };
    struct {
        volatile unsigned                     : 1;
        volatile unsigned ADGO                : 1;
        volatile unsigned CHS                 : 5;
    };
} ADCON0bits @ 0x09D;
#endif

// Register: ADCON1
// Analog-to-Digital Control Register 1
volatile unsigned char           ADCON1              @ 0x09E;
// bit and bitfield definitions
// A/D Positive Voltage Reference Configuration
volatile bit ADPREF0             @ ((unsigned)&ADCON1*8)+0;
// A/D Positive Voltage Reference Configuration
volatile bit ADPREF1             @ ((unsigned)&ADCON1*8)+1;
// A/D Negative Voltage Reference Configuration
volatile bit ADNREF              @ ((unsigned)&ADCON1*8)+2;
// A/D Conversion Clock Select bits
volatile bit ADCS0               @ ((unsigned)&ADCON1*8)+4;
// A/D Conversion Clock Select bits
volatile bit ADCS1               @ ((unsigned)&ADCON1*8)+5;
// A/D Conversion Clock Select bits
volatile bit ADCS2               @ ((unsigned)&ADCON1*8)+6;
// A/D Result Format Select bit
volatile bit ADFM                @ ((unsigned)&ADCON1*8)+7;
#ifndef _LIB_BUILD
union {
    struct {
        volatile unsigned ADPREF0             : 1;
        volatile unsigned ADPREF1             : 1;
        volatile unsigned ADNREF              : 1;
        volatile unsigned                     : 1;
        volatile unsigned ADCS0               : 1;
        volatile unsigned ADCS1               : 1;
        volatile unsigned ADCS2               : 1;
        volatile unsigned ADFM                : 1;
    };
    struct {
        volatile unsigned ADPREF              : 2;
        volatile unsigned                     : 2;
        volatile unsigned ADCS                : 3;
    };
} ADCON1bits @ 0x09E;
#endif

//
// Special function register definitions: Bank 2
//

// Register: LATA
volatile unsigned char           LATA                @ 0x10C;
// bit and bitfield definitions
volatile bit LATA0               @ ((unsigned)&LATA*8)+0;
volatile bit LATA1               @ ((unsigned)&LATA*8)+1;
volatile bit LATA2               @ ((unsigned)&LATA*8)+2;
volatile bit LATA3               @ ((unsigned)&LATA*8)+3;
volatile bit LATA4               @ ((unsigned)&LATA*8)+4;
volatile bit LATA6               @ ((unsigned)&LATA*8)+6;
volatile bit LATA7               @ ((unsigned)&LATA*8)+7;
#ifndef _LIB_BUILD
union {
    struct {
        volatile unsigned LATA0               : 1;
        volatile unsigned LATA1               : 1;
        volatile unsigned LATA2               : 1;
        volatile unsigned LATA3               : 1;
        volatile unsigned LATA4               : 1;
        volatile unsigned                     : 1;
        volatile unsigned LATA6               : 1;
        volatile unsigned LATA7               : 1;
    };
} LATAbits @ 0x10C;
#endif

// Register: LATB
volatile unsigned char           LATB                @ 0x10D;
// bit and bitfield definitions
volatile bit LATB0               @ ((unsigned)&LATB*8)+0;
volatile bit LATB1               @ ((unsigned)&LATB*8)+1;
volatile bit LATB2               @ ((unsigned)&LATB*8)+2;
volatile bit LATB3               @ ((unsigned)&LATB*8)+3;
volatile bit LATB4               @ ((unsigned)&LATB*8)+4;
volatile bit LATB5               @ ((unsigned)&LATB*8)+5;
volatile bit LATB6               @ ((unsigned)&LATB*8)+6;
volatile bit LATB7               @ ((unsigned)&LATB*8)+7;
#ifndef _LIB_BUILD
union {
    struct {
        volatile unsigned LATB0               : 1;
        volatile unsigned LATB1               : 1;
        volatile unsigned LATB2               : 1;
        volatile unsigned LATB3               : 1;
        volatile unsigned LATB4               : 1;
        volatile unsigned LATB5               : 1;
        volatile unsigned LATB6               : 1;
        volatile unsigned LATB7               : 1;
    };
} LATBbits @ 0x10D;
#endif

// Register: CM1CON0
volatile unsigned char           CM1CON0             @ 0x111;
// bit and bitfield definitions
// Comparator Output Synchronous Mode bit
volatile bit C1SYNC              @ ((unsigned)&CM1CON0*8)+0;
// Comparator Hysteresis Enable bit
volatile bit C1HYS               @ ((unsigned)&CM1CON0*8)+1;
// Comparator Speed/Power Select bit
volatile bit C1SP                @ ((unsigned)&CM1CON0*8)+2;
// Comparator Output Polarity Select bit
volatile bit C1POL               @ ((unsigned)&CM1CON0*8)+4;
// Comparator Output Enable bit
volatile bit C1OE                @ ((unsigned)&CM1CON0*8)+5;
// Comparator Output bit
volatile bit C1OUT               @ ((unsigned)&CM1CON0*8)+6;
// Comparator Enable bit
volatile bit C1ON                @ ((unsigned)&CM1CON0*8)+7;
#ifndef _LIB_BUILD
union {
    struct {
        volatile unsigned C1SYNC              : 1;
        volatile unsigned C1HYS               : 1;
        volatile unsigned C1SP                : 1;
        volatile unsigned                     : 1;
        volatile unsigned C1POL               : 1;
        volatile unsigned C1OE                : 1;
        volatile unsigned C1OUT               : 1;
        volatile unsigned C1ON                : 1;
    };
} CM1CON0bits @ 0x111;
#endif

// Register: CM1CON1
volatile unsigned char           CM1CON1             @ 0x112;
// bit and bitfield definitions
// Comparator Negative Input Channel Select bits
volatile bit C1NCH0              @ ((unsigned)&CM1CON1*8)+0;
// Comparator Negative Input Channel Select bits
volatile bit C1NCH1              @ ((unsigned)&CM1CON1*8)+1;
// Comparator Positive Input Channel Select bits
volatile bit C1PCH0              @ ((unsigned)&CM1CON1*8)+4;
// Comparator Positive Input Channel Select bits
volatile bit C1PCH1              @ ((unsigned)&CM1CON1*8)+5;
// Comparator Interrupt on Negative going edge Enable bits
volatile bit C1INTN              @ ((unsigned)&CM1CON1*8)+6;
// Comparator Interrupt on Positive going edge Enable bits
volatile bit C1INTP              @ ((unsigned)&CM1CON1*8)+7;
#ifndef _LIB_BUILD
union {
    struct {
        volatile unsigned C1NCH0              : 1;
        volatile unsigned C1NCH1              : 1;
        volatile unsigned                     : 2;
        volatile unsigned C1PCH0              : 1;
        volatile unsigned C1PCH1              : 1;
        volatile unsigned C1INTN              : 1;
        volatile unsigned C1INTP              : 1;
    };
    struct {
        volatile unsigned C1NCH               : 2;
        volatile unsigned                     : 2;
        volatile unsigned C1PCH               : 2;
    };
} CM1CON1bits @ 0x112;
#endif

// Register: CM2CON0
volatile unsigned char           CM2CON0             @ 0x113;
// bit and bitfield definitions
volatile bit C2SYNC              @ ((unsigned)&CM2CON0*8)+0;
volatile bit C2HYS               @ ((unsigned)&CM2CON0*8)+1;
volatile bit C2SP                @ ((unsigned)&CM2CON0*8)+2;
volatile bit C2POL               @ ((unsigned)&CM2CON0*8)+4;
volatile bit C2OE                @ ((unsigned)&CM2CON0*8)+5;
volatile bit C2OUT               @ ((unsigned)&CM2CON0*8)+6;
volatile bit C2ON                @ ((unsigned)&CM2CON0*8)+7;
#ifndef _LIB_BUILD
union {
    struct {
        volatile unsigned C2SYNC              : 1;
        volatile unsigned C2HYS               : 1;
        volatile unsigned C2SP                : 1;
        volatile unsigned                     : 1;
        volatile unsigned C2POL               : 1;
        volatile unsigned C2OE                : 1;
        volatile unsigned C2OUT               : 1;
        volatile unsigned C2ON                : 1;
    };
} CM2CON0bits @ 0x113;
#endif

// Register: CM2CON1
volatile unsigned char           CM2CON1             @ 0x114;
// bit and bitfield definitions
// Comparator Negative Input Channel Select bits
volatile bit C2NCH0              @ ((unsigned)&CM2CON1*8)+0;
// Comparator Negative Input Channel Select bits
volatile bit C2NCH1              @ ((unsigned)&CM2CON1*8)+1;
// Comparator Positive Input Channel Select bits
volatile bit C2PCH0              @ ((unsigned)&CM2CON1*8)+4;
// Comparator Positive Input Channel Select bits
volatile bit C2PCH1              @ ((unsigned)&CM2CON1*8)+5;
// Comparator Interrupt on Negative going edge Enable bits
volatile bit C2INTN              @ ((unsigned)&CM2CON1*8)+6;
// Comparator Interrupt on Positive going edge Enable bits
volatile bit C2INTP              @ ((unsigned)&CM2CON1*8)+7;
#ifndef _LIB_BUILD
union {
    struct {
        volatile unsigned C2NCH0              : 1;
        volatile unsigned C2NCH1              : 1;
        volatile unsigned                     : 2;
        volatile unsigned C2PCH0              : 1;
        volatile unsigned C2PCH1              : 1;
        volatile unsigned C2INTN              : 1;
        volatile unsigned C2INTP              : 1;
    };
    struct {
        volatile unsigned C2NCH               : 2;
        volatile unsigned                     : 2;
        volatile unsigned C2PCH               : 2;
    };
} CM2CON1bits @ 0x114;
#endif

// Register: CMOUT
// Comparator Output Register
volatile unsigned char           CMOUT               @ 0x115;
// bit and bitfield definitions
volatile bit MC1OUT              @ ((unsigned)&CMOUT*8)+0;
volatile bit MC2OUT              @ ((unsigned)&CMOUT*8)+1;
#ifndef _LIB_BUILD
union {
    struct {
        volatile unsigned MC1OUT              : 1;
        volatile unsigned MC2OUT              : 1;
    };
} CMOUTbits @ 0x115;
#endif

// Register: BORCON
// Brown-out Reset Control Register
volatile unsigned char           BORCON              @ 0x116;
// bit and bitfield definitions
// Brown-out Reset Circuit Ready Status bit
volatile bit BORRDY              @ ((unsigned)&BORCON*8)+0;
// Software Brown Out Reset Enable bit
volatile bit SBOREN              @ ((unsigned)&BORCON*8)+7;
#ifndef _LIB_BUILD
union {
    struct {
        volatile unsigned BORRDY              : 1;
        volatile unsigned                     : 6;
        volatile unsigned SBOREN              : 1;
    };
} BORCONbits @ 0x116;
#endif

// Register: FVRCON
// Fixed Voltage Reference Control Register
volatile unsigned char           FVRCON              @ 0x117;
// bit and bitfield definitions
// A/D Converter Fixed Voltage Reference Selection
volatile bit ADFVR0              @ ((unsigned)&FVRCON*8)+0;
// A/D Converter Fixed Voltage Reference Selection
volatile bit ADFVR1              @ ((unsigned)&FVRCON*8)+1;
// Comparator and D/A Converter Fixed Voltage Reference Selection
volatile bit CDAFVR0             @ ((unsigned)&FVRCON*8)+2;
// Comparator and D/A Converter Fixed Voltage Reference Selection
volatile bit CDAFVR1             @ ((unsigned)&FVRCON*8)+3;
// Fixed Voltage Reference Ready Flag
volatile bit FVRRDY              @ ((unsigned)&FVRCON*8)+6;
// Fixed Voltage Reference Enable
volatile bit FVREN               @ ((unsigned)&FVRCON*8)+7;
#ifndef _LIB_BUILD
union {
    struct {
        volatile unsigned ADFVR0              : 1;
        volatile unsigned ADFVR1              : 1;
        volatile unsigned CDAFVR0             : 1;
        volatile unsigned CDAFVR1             : 1;
        volatile unsigned                     : 1;
        volatile unsigned                     : 1;
        volatile unsigned FVRRDY              : 1;
        volatile unsigned FVREN               : 1;
    };
    struct {
        volatile unsigned ADFVR               : 2;
        volatile unsigned CDAFVR              : 2;
    };
} FVRCONbits @ 0x117;
#endif

// Register: DACCON0
// Voltage Reference Control Register 0
volatile unsigned char           DACCON0             @ 0x118;
// bit and bitfield definitions
// DAC1 Negative Source Select bits
volatile bit DACNSS              @ ((unsigned)&DACCON0*8)+0;
// DAC1 Positive Source Select bits
volatile bit DACPSS0             @ ((unsigned)&DACCON0*8)+2;
// DAC1 Positive Source Select bits
volatile bit DACPSS1             @ ((unsigned)&DACCON0*8)+3;
// DAC1 Voltage Output Enable bit
volatile bit DACOE               @ ((unsigned)&DACCON0*8)+5;
// DAC 1 Low Power Voltage State Select bit
volatile bit DACLPS              @ ((unsigned)&DACCON0*8)+6;
// DAC 1 Enable bit
volatile bit DACEN               @ ((unsigned)&DACCON0*8)+7;
#ifndef _LIB_BUILD
union {
    struct {
        volatile unsigned DACNSS              : 1;
        volatile unsigned                     : 1;
        volatile unsigned DACPSS0             : 1;
        volatile unsigned DACPSS1             : 1;
        volatile unsigned                     : 1;
        volatile unsigned DACOE               : 1;
        volatile unsigned DACLPS              : 1;
        volatile unsigned DACEN               : 1;
    };
    struct {
        volatile unsigned                     : 2;
        volatile unsigned DACPSS              : 2;
    };
} DACCON0bits @ 0x118;
#endif

// Register: DACCON1
// Voltage Reference Control Register 1
volatile unsigned char           DACCON1             @ 0x119;
// bit and bitfield definitions
// DAC1 Voltage Output Select bits
volatile bit DACR0               @ ((unsigned)&DACCON1*8)+0;
// DAC1 Voltage Output Select bits
volatile bit DACR1               @ ((unsigned)&DACCON1*8)+1;
// DAC1 Voltage Output Select bits
volatile bit DACR2               @ ((unsigned)&DACCON1*8)+2;
// DAC1 Voltage Output Select bits
volatile bit DACR3               @ ((unsigned)&DACCON1*8)+3;
// DAC1 Voltage Output Select bits
volatile bit DACR4               @ ((unsigned)&DACCON1*8)+4;
#ifndef _LIB_BUILD
union {
    struct {
        volatile unsigned DACR0               : 1;
        volatile unsigned DACR1               : 1;
        volatile unsigned DACR2               : 1;
        volatile unsigned DACR3               : 1;
        volatile unsigned DACR4               : 1;
        volatile unsigned                     : 1;
    };
    struct {
        volatile unsigned DACR                : 5;
    };
} DACCON1bits @ 0x119;
#endif

// Register: SRCON0
// SR Latch Control Register 0
volatile unsigned char           SRCON0              @ 0x11A;
// bit and bitfield definitions
// Pulse Reset Input of the SR Latch
volatile bit SRPR                @ ((unsigned)&SRCON0*8)+0;
// Pulse Set Input of the SR Latch
volatile bit SRPS                @ ((unsigned)&SRCON0*8)+1;
// SR Latch Q Output Enable bit
volatile bit SRNQEN              @ ((unsigned)&SRCON0*8)+2;
// SR Latch Q Output Enable bit
volatile bit SRQEN               @ ((unsigned)&SRCON0*8)+3;
// SR Latch Clock divider bits
volatile bit SRCLK0              @ ((unsigned)&SRCON0*8)+4;
// SR Latch Clock divider bits
volatile bit SRCLK1              @ ((unsigned)&SRCON0*8)+5;
// SR Latch Clock divider bits
volatile bit SRCLK2              @ ((unsigned)&SRCON0*8)+6;
// SR Latch Enable bit
volatile bit SRLEN               @ ((unsigned)&SRCON0*8)+7;
#ifndef _LIB_BUILD
union {
    struct {
        volatile unsigned SRPR                : 1;
        volatile unsigned SRPS                : 1;
        volatile unsigned SRNQEN              : 1;
        volatile unsigned SRQEN               : 1;
        volatile unsigned SRCLK0              : 1;
        volatile unsigned SRCLK1              : 1;
        volatile unsigned SRCLK2              : 1;
        volatile unsigned SRLEN               : 1;
    };
    struct {
        volatile unsigned                     : 4;
        volatile unsigned SRCLK               : 3;
    };
} SRCON0bits @ 0x11A;
#endif

// Register: SRCON1
// SR Latch Control Register 1
volatile unsigned char           SRCON1              @ 0x11B;
// bit and bitfield definitions
// SR Latch C1 Reset Enable bit
volatile bit SRRC1E              @ ((unsigned)&SRCON1*8)+0;
// SR Latch C2 Reset Enable bit
volatile bit SRRC2E              @ ((unsigned)&SRCON1*8)+1;
// SR Latch Reset Clock Enable bit
volatile bit SRRCKE              @ ((unsigned)&SRCON1*8)+2;
// SR Latch Peripheral Reset Enable bit
volatile bit SRRPE               @ ((unsigned)&SRCON1*8)+3;
// SR Latch C1 Set Enable bit
volatile bit SRSC1E              @ ((unsigned)&SRCON1*8)+4;
// SR Latch C2 Set Enable bit
volatile bit SRSC2E              @ ((unsigned)&SRCON1*8)+5;
// SR Latch Set Clock Enable bit
volatile bit SRSCKE              @ ((unsigned)&SRCON1*8)+6;
// SR Latch Peripheral Set Enable bit
volatile bit SRSPE               @ ((unsigned)&SRCON1*8)+7;
#ifndef _LIB_BUILD
union {
    struct {
        volatile unsigned SRRC1E              : 1;
        volatile unsigned SRRC2E              : 1;
        volatile unsigned SRRCKE              : 1;
        volatile unsigned SRRPE               : 1;
        volatile unsigned SRSC1E              : 1;
        volatile unsigned SRSC2E              : 1;
        volatile unsigned SRSCKE              : 1;
        volatile unsigned SRSPE               : 1;
    };
} SRCON1bits @ 0x11B;
#endif

// Register: APFCON0
// Alternate Pin Function Control Register
volatile unsigned char           APFCON0             @ 0x11D;
// bit and bitfield definitions
// CCP2 Input/Output Pin Selection
volatile bit CCP1SEL             @ ((unsigned)&APFCON0*8)+0;
// SS Input Pin Selection
volatile bit P1CSEL              @ ((unsigned)&APFCON0*8)+1;
// Comparator 2 Output Pin Selection
volatile bit P1DSEL              @ ((unsigned)&APFCON0*8)+2;
// SR Latch nQ Output Pin Selection
volatile bit CCP2SEL             @ ((unsigned)&APFCON0*8)+3;
// CCP2 PWM B Output Pin Selection
volatile bit P2BSEL              @ ((unsigned)&APFCON0*8)+4;
// Timer1 Gate Input Pin Selection
volatile bit SS1SEL              @ ((unsigned)&APFCON0*8)+5;
// CCP3 Input/Output Pin Selection
volatile bit SDO1SEL             @ ((unsigned)&APFCON0*8)+6;
volatile bit RXDTSEL             @ ((unsigned)&APFCON0*8)+7;
#ifndef _LIB_BUILD
union {
    struct {
        volatile unsigned CCP1SEL             : 1;
        volatile unsigned P1CSEL              : 1;
        volatile unsigned P1DSEL              : 1;
        volatile unsigned CCP2SEL             : 1;
        volatile unsigned P2BSEL              : 1;
        volatile unsigned SS1SEL              : 1;
        volatile unsigned SDO1SEL             : 1;
        volatile unsigned RXDTSEL             : 1;
    };
} APFCON0bits @ 0x11D;
#endif

// Register: APFCON1
volatile unsigned char           APFCON1             @ 0x11E;
// bit and bitfield definitions
volatile bit TXCKSEL             @ ((unsigned)&APFCON1*8)+0;
#ifndef _LIB_BUILD
union {
    struct {
        volatile unsigned TXCKSEL             : 1;
    };
} APFCON1bits @ 0x11E;
#endif

//
// Special function register definitions: Bank 3
//

// Register: ANSELA
volatile unsigned char           ANSELA              @ 0x18C;
// bit and bitfield definitions
volatile bit ANSA0               @ ((unsigned)&ANSELA*8)+0;
volatile bit ANSA1               @ ((unsigned)&ANSELA*8)+1;
volatile bit ANSA2               @ ((unsigned)&ANSELA*8)+2;
volatile bit ANSA3               @ ((unsigned)&ANSELA*8)+3;
volatile bit ANSA4               @ ((unsigned)&ANSELA*8)+4;
#ifndef _LIB_BUILD
union {
    struct {
        volatile unsigned ANSA0               : 1;
        volatile unsigned ANSA1               : 1;
        volatile unsigned ANSA2               : 1;
        volatile unsigned ANSA3               : 1;
        volatile unsigned ANSA4               : 1;
        volatile unsigned                     : 1;
        volatile unsigned                     : 1;
        volatile unsigned                     : 1;
    };
    struct {
        volatile unsigned ANSELA              : 5;
        volatile unsigned                     : 1;
    };
} ANSELAbits @ 0x18C;
#endif

// Register: ANSELB
volatile unsigned char           ANSELB              @ 0x18D;
// bit and bitfield definitions
volatile bit ANSB1               @ ((unsigned)&ANSELB*8)+1;
volatile bit ANSB2               @ ((unsigned)&ANSELB*8)+2;
volatile bit ANSB3               @ ((unsigned)&ANSELB*8)+3;
volatile bit ANSB4               @ ((unsigned)&ANSELB*8)+4;
volatile bit ANSB5               @ ((unsigned)&ANSELB*8)+5;
volatile bit ANSB6               @ ((unsigned)&ANSELB*8)+6;
volatile bit ANSB7               @ ((unsigned)&ANSELB*8)+7;
#ifndef _LIB_BUILD
union {
    struct {
        volatile unsigned                     : 1;
        volatile unsigned ANSB1               : 1;
        volatile unsigned ANSB2               : 1;
        volatile unsigned ANSB3               : 1;
        volatile unsigned ANSB4               : 1;
        volatile unsigned ANSB5               : 1;
        volatile unsigned ANSB6               : 1;
        volatile unsigned ANSB7               : 1;
    };
} ANSELBbits @ 0x18D;
#endif
// bit and bitfield definitions

// Register: EEADRL
volatile unsigned char           EEADRL              @ 0x191;
// bit and bitfield definitions

// Register: EEADRH
volatile unsigned char           EEADRH              @ 0x192;
// bit and bitfield definitions
#ifndef _LIB_BUILD
union {
    struct {
        volatile unsigned EEADRH              : 7;
    };
} EEADRHbits @ 0x192;
#endif

// Register: EEADR
volatile unsigned int            EEADR               @ 0x191;
// bit and bitfield definitions

// Register: EEDATL
volatile unsigned char           EEDATL              @ 0x193;
volatile unsigned char           EEDATA              @ 0x193;
// bit and bitfield definitions

// Register: EEDATH
volatile unsigned char           EEDATH              @ 0x194;
// bit and bitfield definitions
#ifndef _LIB_BUILD
union {
    struct {
        volatile unsigned EEDATH              : 6;
    };
} EEDATHbits @ 0x194;
#endif

// Register: EEDAT
volatile unsigned int            EEDAT               @ 0x193;

// Register: EECON1
volatile unsigned char           EECON1              @ 0x195;
// bit and bitfield definitions
// Read Control bit
volatile bit RD                  @ ((unsigned)&EECON1*8)+0;
// Write Control bit
volatile bit WR                  @ ((unsigned)&EECON1*8)+1;
// Program/Erase Enable bit
volatile bit WREN                @ ((unsigned)&EECON1*8)+2;
// Sequence Error Flag bit
volatile bit WRERR               @ ((unsigned)&EECON1*8)+3;
// Program FLASH Erase Enable bit
volatile bit FREE                @ ((unsigned)&EECON1*8)+4;
// Load Write Latches Only bit
volatile bit LWLO                @ ((unsigned)&EECON1*8)+5;
// FLASH Program / Data EEPROM or Configuration Select bit
volatile bit CFGS                @ ((unsigned)&EECON1*8)+6;
// FLASH Program / Data EEPROM Memory Select bit
volatile bit EEPGD               @ ((unsigned)&EECON1*8)+7;
#ifndef _LIB_BUILD
union {
    struct {
        volatile unsigned RD                  : 1;
        volatile unsigned WR                  : 1;
        volatile unsigned WREN                : 1;
        volatile unsigned WRERR               : 1;
        volatile unsigned FREE                : 1;
        volatile unsigned LWLO                : 1;
        volatile unsigned CFGS                : 1;
        volatile unsigned EEPGD               : 1;
    };
} EECON1bits @ 0x195;
#endif

// Register: EECON2
volatile unsigned char           EECON2              @ 0x196;
// bit and bitfield definitions

// Register: RCREG
volatile unsigned char           RCREG               @ 0x199;
// bit and bitfield definitions

// Register: TXREG
volatile unsigned char           TXREG               @ 0x19A;
// bit and bitfield definitions

// Register: SPBRGL
volatile unsigned char           SPBRGL              @ 0x19B;
volatile unsigned char           SPBRG               @ 0x19B;
// bit and bitfield definitions

// Register: SPBRGH
volatile unsigned char           SPBRGH              @ 0x19C;
// bit and bitfield definitions

// Register: RCSTA
// Receive Status and Control Register
volatile unsigned char           RCSTA               @ 0x19D;
// bit and bitfield definitions
// 9th bit of received data (can be parity bit)
volatile bit RX9D                @ ((unsigned)&RCSTA*8)+0;
// Overrun Error bit
volatile bit OERR                @ ((unsigned)&RCSTA*8)+1;
// Framing Error bit
volatile bit FERR                @ ((unsigned)&RCSTA*8)+2;
// Address Detect Enable bit
volatile bit ADDEN               @ ((unsigned)&RCSTA*8)+3;
// Continuous Receive Enable bit
volatile bit CREN                @ ((unsigned)&RCSTA*8)+4;
// Single Receive Enable bit
volatile bit SREN                @ ((unsigned)&RCSTA*8)+5;
// 9-bit Receive Enable bit
volatile bit RX9                 @ ((unsigned)&RCSTA*8)+6;
// Serial Port Enable bit
volatile bit SPEN                @ ((unsigned)&RCSTA*8)+7;
#ifndef _LIB_BUILD
union {
    struct {
        volatile unsigned RX9D                : 1;
        volatile unsigned OERR                : 1;
        volatile unsigned FERR                : 1;
        volatile unsigned ADDEN               : 1;
        volatile unsigned CREN                : 1;
        volatile unsigned SREN                : 1;
        volatile unsigned RX9                 : 1;
        volatile unsigned SPEN                : 1;
    };
} RCSTAbits @ 0x19D;
#endif

// Register: TXSTA
// Transmit Status and Control Register
volatile unsigned char           TXSTA               @ 0x19E;
// bit and bitfield definitions
// 9th bit of transmit data; can be used as parity bit
volatile bit TX9D                @ ((unsigned)&TXSTA*8)+0;
// Transmit Operation Idle Status bit
volatile bit TRMT                @ ((unsigned)&TXSTA*8)+1;
// High Baud Rate Select bit
volatile bit BRGH                @ ((unsigned)&TXSTA*8)+2;
// Send BREAK character bit
volatile bit SENDB               @ ((unsigned)&TXSTA*8)+3;
// USART Mode Select bit
volatile bit SYNC                @ ((unsigned)&TXSTA*8)+4;
// Transmit Enable bit
volatile bit TXEN                @ ((unsigned)&TXSTA*8)+5;
// 9-bit Transmit Enable bit
volatile bit TX9                 @ ((unsigned)&TXSTA*8)+6;
// Clock Source Select bit
volatile bit CSRC                @ ((unsigned)&TXSTA*8)+7;
#ifndef _LIB_BUILD
union {
    struct {
        volatile unsigned TX9D                : 1;
        volatile unsigned TRMT                : 1;
        volatile unsigned BRGH                : 1;
        volatile unsigned SENDB               : 1;
        volatile unsigned SYNC                : 1;
        volatile unsigned TXEN                : 1;
        volatile unsigned TX9                 : 1;
        volatile unsigned CSRC                : 1;
    };
} TXSTAbits @ 0x19E;
#endif

// Register: BAUDCON
// Baud Rate Control Register
volatile unsigned char           BAUDCON             @ 0x19F;
// bit and bitfield definitions
// Auto-Baud Detect Enable bit
volatile bit ABDEN               @ ((unsigned)&BAUDCON*8)+0;
// Wake-Up Enable bit
volatile bit WUE                 @ ((unsigned)&BAUDCON*8)+1;
// 16-bit Baud Rate Register enable bit
volatile bit BRG16               @ ((unsigned)&BAUDCON*8)+3;
// Clock/Transmit Polarity Select bit
volatile bit SCKP                @ ((unsigned)&BAUDCON*8)+4;
// Receive Operation Idle Status bit
volatile bit RCIDL               @ ((unsigned)&BAUDCON*8)+6;
// BRG Rollover Status bit
volatile bit ABDOVF              @ ((unsigned)&BAUDCON*8)+7;
#ifndef _LIB_BUILD
union {
    struct {
        volatile unsigned ABDEN               : 1;
        volatile unsigned WUE                 : 1;
        volatile unsigned                     : 1;
        volatile unsigned BRG16               : 1;
        volatile unsigned SCKP                : 1;
        volatile unsigned                     : 1;
        volatile unsigned RCIDL               : 1;
        volatile unsigned ABDOVF              : 1;
    };
} BAUDCONbits @ 0x19F;
#endif

//
// Special function register definitions: Bank 4
//

// Register: WPUA
volatile unsigned char           WPUA                @ 0x20C;
// bit and bitfield definitions
volatile bit WPUA5               @ ((unsigned)&WPUA*8)+5;
#ifndef _LIB_BUILD
union {
    struct {
        volatile unsigned                     : 5;
        volatile unsigned WPUA5               : 1;
    };
} WPUAbits @ 0x20C;
#endif

// Register: WPUB
volatile unsigned char           WPUB                @ 0x20D;
// bit and bitfield definitions
volatile bit WPUB0               @ ((unsigned)&WPUB*8)+0;
volatile bit WPUB1               @ ((unsigned)&WPUB*8)+1;
volatile bit WPUB2               @ ((unsigned)&WPUB*8)+2;
volatile bit WPUB3               @ ((unsigned)&WPUB*8)+3;
volatile bit WPUB4               @ ((unsigned)&WPUB*8)+4;
volatile bit WPUB5               @ ((unsigned)&WPUB*8)+5;
volatile bit WPUB6               @ ((unsigned)&WPUB*8)+6;
volatile bit WPUB7               @ ((unsigned)&WPUB*8)+7;
#ifndef _LIB_BUILD
union {
    struct {
        volatile unsigned WPUB0               : 1;
        volatile unsigned WPUB1               : 1;
        volatile unsigned WPUB2               : 1;
        volatile unsigned WPUB3               : 1;
        volatile unsigned WPUB4               : 1;
        volatile unsigned WPUB5               : 1;
        volatile unsigned WPUB6               : 1;
        volatile unsigned WPUB7               : 1;
    };
} WPUBbits @ 0x20D;
#endif

// Register: SSP1BUF
volatile unsigned char           SSP1BUF             @ 0x211;
volatile unsigned char           SSPBUF              @ 0x211;
// bit and bitfield definitions

// Register: SSP1ADD
volatile unsigned char           SSP1ADD             @ 0x212;
volatile unsigned char           SSPADD              @ 0x212;
// bit and bitfield definitions

// Register: SSP1MSK
volatile unsigned char           SSP1MSK             @ 0x213;
volatile unsigned char           SSPMSK              @ 0x213;
// bit and bitfield definitions

// Register: SSP1STAT
volatile unsigned char           SSP1STAT            @ 0x214;
volatile unsigned char           SSPSTAT             @ 0x214;
// bit and bitfield definitions
volatile bit SSP1STAT_BF                  @ ((unsigned)&SSP1STAT*8)+0;
volatile bit SSP1STAT_UA                  @ ((unsigned)&SSP1STAT*8)+1;
volatile bit SSP1STAT_R_nW                @ ((unsigned)&SSP1STAT*8)+2;
volatile bit SSP1STAT_S                   @ ((unsigned)&SSP1STAT*8)+3;
volatile bit SSP1STAT_P                   @ ((unsigned)&SSP1STAT*8)+4;
volatile bit SSP1STAT_D_nA                @ ((unsigned)&SSP1STAT*8)+5;
volatile bit SSP1STAT_CKE                 @ ((unsigned)&SSP1STAT*8)+6;
volatile bit SSP1STAT_SMP                 @ ((unsigned)&SSP1STAT*8)+7;
#ifndef _LIB_BUILD
union {
    struct {
        volatile unsigned BF                  : 1;
        volatile unsigned UA                  : 1;
        volatile unsigned R_nW                : 1;
        volatile unsigned S                   : 1;
        volatile unsigned P                   : 1;
        volatile unsigned D_nA                : 1;
        volatile unsigned CKE                 : 1;
        volatile unsigned SMP                 : 1;
    };
} SSP1STATbits @ 0x214;
#endif

// Register: SSP1CON1
volatile unsigned char           SSP1CON1            @ 0x215;
volatile unsigned char           SSPCON1             @ 0x215;
volatile unsigned char           SSPCON              @ 0x215;
// bit and bitfield definitions
volatile bit SSP1CON1_SSPM0               @ ((unsigned)&SSP1CON1*8)+0;
volatile bit SSP1CON1_SSPM1               @ ((unsigned)&SSP1CON1*8)+1;
volatile bit SSP1CON1_SSPM2               @ ((unsigned)&SSP1CON1*8)+2;
volatile bit SSP1CON1_SSPM3               @ ((unsigned)&SSP1CON1*8)+3;
volatile bit SSP1CON1_CKP                 @ ((unsigned)&SSP1CON1*8)+4;
volatile bit SSP1CON1_SSPEN               @ ((unsigned)&SSP1CON1*8)+5;
volatile bit SSP1CON1_SSPOV               @ ((unsigned)&SSP1CON1*8)+6;
volatile bit SSP1CON1_WCOL                @ ((unsigned)&SSP1CON1*8)+7;
#ifndef _LIB_BUILD
union {
    struct {
        volatile unsigned SSPM0               : 1;
        volatile unsigned SSPM1               : 1;
        volatile unsigned SSPM2               : 1;
        volatile unsigned SSPM3               : 1;
        volatile unsigned CKP                 : 1;
        volatile unsigned SSPEN               : 1;
        volatile unsigned SSPOV               : 1;
        volatile unsigned WCOL                : 1;
    };
    struct {
        volatile unsigned SSPM                : 4;
    };
} SSP1CON1bits @ 0x215;
#endif

// Register: SSP1CON2
volatile unsigned char           SSP1CON2            @ 0x216;
volatile unsigned char           SSPCON2             @ 0x216;
// bit and bitfield definitions
volatile bit SSP1CON2_SEN                 @ ((unsigned)&SSP1CON2*8)+0;
volatile bit SSP1CON2_RSEN                @ ((unsigned)&SSP1CON2*8)+1;
volatile bit SSP1CON2_PEN                 @ ((unsigned)&SSP1CON2*8)+2;
volatile bit SSP1CON2_RCEN                @ ((unsigned)&SSP1CON2*8)+3;
volatile bit SSP1CON2_ACKEN               @ ((unsigned)&SSP1CON2*8)+4;
volatile bit SSP1CON2_ACKDT               @ ((unsigned)&SSP1CON2*8)+5;
volatile bit SSP1CON2_ACKSTAT             @ ((unsigned)&SSP1CON2*8)+6;
volatile bit SSP1CON2_GCEN                @ ((unsigned)&SSP1CON2*8)+7;
#ifndef _LIB_BUILD
union {
    struct {
        volatile unsigned SEN                 : 1;
        volatile unsigned RSEN                : 1;
        volatile unsigned PEN                 : 1;
        volatile unsigned RCEN                : 1;
        volatile unsigned ACKEN               : 1;
        volatile unsigned ACKDT               : 1;
        volatile unsigned ACKSTAT             : 1;
        volatile unsigned GCEN                : 1;
    };
} SSP1CON2bits @ 0x216;
#endif

// Register: SSP1CON3
volatile unsigned char           SSP1CON3            @ 0x217;
volatile unsigned char           SSPCON3             @ 0x217;
// bit and bitfield definitions
volatile bit SSP1CON3_DHEN                @ ((unsigned)&SSP1CON3*8)+0;
volatile bit SSP1CON3_AHEN                @ ((unsigned)&SSP1CON3*8)+1;
volatile bit SSP1CON3_SBCDE               @ ((unsigned)&SSP1CON3*8)+2;
volatile bit SSP1CON3_SDAHT               @ ((unsigned)&SSP1CON3*8)+3;
volatile bit SSP1CON3_BOEN                @ ((unsigned)&SSP1CON3*8)+4;
volatile bit SSP1CON3_SCIE                @ ((unsigned)&SSP1CON3*8)+5;
volatile bit SSP1CON3_PCIE                @ ((unsigned)&SSP1CON3*8)+6;
volatile bit SSP1CON3_ACKTIM              @ ((unsigned)&SSP1CON3*8)+7;
#ifndef _LIB_BUILD
union {
    struct {
        volatile unsigned DHEN                : 1;
        volatile unsigned AHEN                : 1;
        volatile unsigned SBCDE               : 1;
        volatile unsigned SDAHT               : 1;
        volatile unsigned BOEN                : 1;
        volatile unsigned SCIE                : 1;
        volatile unsigned PCIE                : 1;
        volatile unsigned ACKTIM              : 1;
    };
} SSP1CON3bits @ 0x217;
#endif

// Register: SSP2BUF
volatile unsigned char           SSP2BUF             @ 0x219;
// bit and bitfield definitions

// Register: SSP2ADD
volatile unsigned char           SSP2ADD             @ 0x21A;
// bit and bitfield definitions

// Register: SSP2MSK
volatile unsigned char           SSP2MSK             @ 0x21B;
// bit and bitfield definitions

// Register: SSP2STAT
volatile unsigned char           SSP2STAT            @ 0x21C;
// bit and bitfield definitions
volatile bit SSP2STAT_BF                  @ ((unsigned)&SSP2STAT*8)+0;
volatile bit SSP2STAT_UA                  @ ((unsigned)&SSP2STAT*8)+1;
volatile bit SSP2STAT_R_nW                @ ((unsigned)&SSP2STAT*8)+2;
volatile bit SSP2STAT_S                   @ ((unsigned)&SSP2STAT*8)+3;
volatile bit SSP2STAT_P                   @ ((unsigned)&SSP2STAT*8)+4;
volatile bit SSP2STAT_D_nA                @ ((unsigned)&SSP2STAT*8)+5;
volatile bit SSP2STAT_CKE                 @ ((unsigned)&SSP2STAT*8)+6;
volatile bit SSP2STAT_SMP                 @ ((unsigned)&SSP2STAT*8)+7;
#ifndef _LIB_BUILD
union {
    struct {
        volatile unsigned BF                  : 1;
        volatile unsigned UA                  : 1;
        volatile unsigned R_nW                : 1;
        volatile unsigned S                   : 1;
        volatile unsigned P                   : 1;
        volatile unsigned D_nA                : 1;
        volatile unsigned CKE                 : 1;
        volatile unsigned SMP                 : 1;
    };
} SSP2STATbits @ 0x21C;
#endif

// Register: SSP2CON1
volatile unsigned char           SSP2CON1            @ 0x21D;
// bit and bitfield definitions
volatile bit SSP2CON1_SSPM0               @ ((unsigned)&SSP2CON1*8)+0;
volatile bit SSP2CON1_SSPM1               @ ((unsigned)&SSP2CON1*8)+1;
volatile bit SSP2CON1_SSPM2               @ ((unsigned)&SSP2CON1*8)+2;
volatile bit SSP2CON1_SSPM3               @ ((unsigned)&SSP2CON1*8)+3;
volatile bit SSP2CON1_CKP                 @ ((unsigned)&SSP2CON1*8)+4;
volatile bit SSP2CON1_SSPEN               @ ((unsigned)&SSP2CON1*8)+5;
volatile bit SSP2CON1_SSPOV               @ ((unsigned)&SSP2CON1*8)+6;
volatile bit SSP2CON1_WCOL                @ ((unsigned)&SSP2CON1*8)+7;
#ifndef _LIB_BUILD
union {
    struct {
        volatile unsigned SSPM0               : 1;
        volatile unsigned SSPM1               : 1;
        volatile unsigned SSPM2               : 1;
        volatile unsigned SSPM3               : 1;
        volatile unsigned CKP                 : 1;
        volatile unsigned SSPEN               : 1;
        volatile unsigned SSPOV               : 1;
        volatile unsigned WCOL                : 1;
    };
    struct {
        volatile unsigned SSPM                : 4;
    };
} SSP2CON1bits @ 0x21D;
#endif

// Register: SSP2CON2
volatile unsigned char           SSP2CON2            @ 0x21E;
// bit and bitfield definitions
volatile bit SSP2CON2_SEN                 @ ((unsigned)&SSP2CON2*8)+0;
volatile bit SSP2CON2_RSEN                @ ((unsigned)&SSP2CON2*8)+1;
volatile bit SSP2CON2_PEN                 @ ((unsigned)&SSP2CON2*8)+2;
volatile bit SSP2CON2_RCEN                @ ((unsigned)&SSP2CON2*8)+3;
volatile bit SSP2CON2_ACKEN               @ ((unsigned)&SSP2CON2*8)+4;
volatile bit SSP2CON2_ACKDT               @ ((unsigned)&SSP2CON2*8)+5;
volatile bit SSP2CON2_ACKSTAT             @ ((unsigned)&SSP2CON2*8)+6;
volatile bit SSP2CON2_GCEN                @ ((unsigned)&SSP2CON2*8)+7;
#ifndef _LIB_BUILD
union {
    struct {
        volatile unsigned SEN                 : 1;
        volatile unsigned RSEN                : 1;
        volatile unsigned PEN                 : 1;
        volatile unsigned RCEN                : 1;
        volatile unsigned ACKEN               : 1;
        volatile unsigned ACKDT               : 1;
        volatile unsigned ACKSTAT             : 1;
        volatile unsigned GCEN                : 1;
    };
} SSP2CON2bits @ 0x21E;
#endif

// Register: SSP2CON3
volatile unsigned char           SSP2CON3            @ 0x21F;
// bit and bitfield definitions
volatile bit SSP2CON3_DHEN                @ ((unsigned)&SSP2CON3*8)+0;
volatile bit SSP2CON3_AHEN                @ ((unsigned)&SSP2CON3*8)+1;
volatile bit SSP2CON3_SBCDE               @ ((unsigned)&SSP2CON3*8)+2;
volatile bit SSP2CON3_SDAHT               @ ((unsigned)&SSP2CON3*8)+3;
volatile bit SSP2CON3_BOEN                @ ((unsigned)&SSP2CON3*8)+4;
volatile bit SSP2CON3_SCIE                @ ((unsigned)&SSP2CON3*8)+5;
volatile bit SSP2CON3_PCIE                @ ((unsigned)&SSP2CON3*8)+6;
volatile bit SSP2CON3_ACKTIM              @ ((unsigned)&SSP2CON3*8)+7;
#ifndef _LIB_BUILD
union {
    struct {
        volatile unsigned DHEN                : 1;
        volatile unsigned AHEN                : 1;
        volatile unsigned SBCDE               : 1;
        volatile unsigned SDAHT               : 1;
        volatile unsigned BOEN                : 1;
        volatile unsigned SCIE                : 1;
        volatile unsigned PCIE                : 1;
        volatile unsigned ACKTIM              : 1;
    };
} SSP2CON3bits @ 0x21F;
#endif

//
// Special function register definitions: Bank 5
//

// Register: CCPR1L
volatile unsigned char           CCPR1L              @ 0x291;
// bit and bitfield definitions

// Register: CCPR1H
volatile unsigned char           CCPR1H              @ 0x292;
// bit and bitfield definitions

// Register: CCP1CON
volatile unsigned char           CCP1CON             @ 0x293;
// bit and bitfield definitions
volatile bit CCP1M0              @ ((unsigned)&CCP1CON*8)+0;
volatile bit CCP1M1              @ ((unsigned)&CCP1CON*8)+1;
volatile bit CCP1M2              @ ((unsigned)&CCP1CON*8)+2;
volatile bit CCP1M3              @ ((unsigned)&CCP1CON*8)+3;
volatile bit DC1B0               @ ((unsigned)&CCP1CON*8)+4;
volatile bit DC1B1               @ ((unsigned)&CCP1CON*8)+5;
volatile bit P1M0                @ ((unsigned)&CCP1CON*8)+6;
volatile bit P1M1                @ ((unsigned)&CCP1CON*8)+7;
#ifndef _LIB_BUILD
union {
    struct {
        volatile unsigned CCP1M0              : 1;
        volatile unsigned CCP1M1              : 1;
        volatile unsigned CCP1M2              : 1;
        volatile unsigned CCP1M3              : 1;
        volatile unsigned DC1B0               : 1;
        volatile unsigned DC1B1               : 1;
        volatile unsigned P1M0                : 1;
        volatile unsigned P1M1                : 1;
    };
    struct {
        volatile unsigned CCP1M               : 4;
        volatile unsigned DC1B                : 2;
        volatile unsigned P1M                 : 2;
    };
} CCP1CONbits @ 0x293;
#endif

// Register: PWM1CON
volatile unsigned char           PWM1CON             @ 0x294;
// bit and bitfield definitions
volatile bit P1DC0               @ ((unsigned)&PWM1CON*8)+0;
volatile bit P1DC1               @ ((unsigned)&PWM1CON*8)+1;
volatile bit P1DC2               @ ((unsigned)&PWM1CON*8)+2;
volatile bit P1DC3               @ ((unsigned)&PWM1CON*8)+3;
volatile bit P1DC4               @ ((unsigned)&PWM1CON*8)+4;
volatile bit P1DC5               @ ((unsigned)&PWM1CON*8)+5;
volatile bit P1DC6               @ ((unsigned)&PWM1CON*8)+6;
volatile bit P1RSEN              @ ((unsigned)&PWM1CON*8)+7;
#ifndef _LIB_BUILD
union {
    struct {
        volatile unsigned P1DC0               : 1;
        volatile unsigned P1DC1               : 1;
        volatile unsigned P1DC2               : 1;
        volatile unsigned P1DC3               : 1;
        volatile unsigned P1DC4               : 1;
        volatile unsigned P1DC5               : 1;
        volatile unsigned P1DC6               : 1;
        volatile unsigned P1RSEN              : 1;
    };
    struct {
        volatile unsigned P1DC                : 7;
    };
} PWM1CONbits @ 0x294;
#endif

// Register: CCP1AS
volatile unsigned char           CCP1AS              @ 0x295;
volatile unsigned char           ECCP1AS             @ 0x295;
// bit and bitfield definitions
volatile bit PSS1BD0             @ ((unsigned)&CCP1AS*8)+0;
volatile bit PSS1BD1             @ ((unsigned)&CCP1AS*8)+1;
volatile bit PSS1AC0             @ ((unsigned)&CCP1AS*8)+2;
volatile bit PSS1AC1             @ ((unsigned)&CCP1AS*8)+3;
volatile bit CCP1AS0             @ ((unsigned)&CCP1AS*8)+4;
volatile bit CCP1AS1             @ ((unsigned)&CCP1AS*8)+5;
volatile bit CCP1AS2             @ ((unsigned)&CCP1AS*8)+6;
volatile bit CCP1ASE             @ ((unsigned)&CCP1AS*8)+7;
#ifndef _LIB_BUILD
union {
    struct {
        volatile unsigned PSS1BD0             : 1;
        volatile unsigned PSS1BD1             : 1;
        volatile unsigned PSS1AC0             : 1;
        volatile unsigned PSS1AC1             : 1;
        volatile unsigned CCP1AS0             : 1;
        volatile unsigned CCP1AS1             : 1;
        volatile unsigned CCP1AS2             : 1;
        volatile unsigned CCP1ASE             : 1;
    };
    struct {
        volatile unsigned PSS1BD              : 2;
        volatile unsigned PSS1AC              : 2;
        volatile unsigned CCP1AS              : 3;
    };
} CCP1ASbits @ 0x295;
#endif

// Register: PSTR1CON
volatile unsigned char           PSTR1CON            @ 0x296;
// bit and bitfield definitions
volatile bit STR1A               @ ((unsigned)&PSTR1CON*8)+0;
volatile bit STR1B               @ ((unsigned)&PSTR1CON*8)+1;
volatile bit STR1C               @ ((unsigned)&PSTR1CON*8)+2;
volatile bit STR1D               @ ((unsigned)&PSTR1CON*8)+3;
volatile bit STR1SYNC            @ ((unsigned)&PSTR1CON*8)+4;
#ifndef _LIB_BUILD
union {
    struct {
        volatile unsigned STR1A               : 1;
        volatile unsigned STR1B               : 1;
        volatile unsigned STR1C               : 1;
        volatile unsigned STR1D               : 1;
        volatile unsigned STR1SYNC            : 1;
    };
} PSTR1CONbits @ 0x296;
#endif

// Register: CCPR2L
volatile unsigned char           CCPR2L              @ 0x298;
// bit and bitfield definitions

// Register: CCPR2H
volatile unsigned char           CCPR2H              @ 0x299;
// bit and bitfield definitions

// Register: CCP2CON
volatile unsigned char           CCP2CON             @ 0x29A;
// bit and bitfield definitions
volatile bit CCP2M0              @ ((unsigned)&CCP2CON*8)+0;
volatile bit CCP2M1              @ ((unsigned)&CCP2CON*8)+1;
volatile bit CCP2M2              @ ((unsigned)&CCP2CON*8)+2;
volatile bit CCP2M3              @ ((unsigned)&CCP2CON*8)+3;
volatile bit DC2B0               @ ((unsigned)&CCP2CON*8)+4;
volatile bit DC2B1               @ ((unsigned)&CCP2CON*8)+5;
volatile bit P2M0                @ ((unsigned)&CCP2CON*8)+6;
volatile bit P2M1                @ ((unsigned)&CCP2CON*8)+7;
#ifndef _LIB_BUILD
union {
    struct {
        volatile unsigned CCP2M0              : 1;
        volatile unsigned CCP2M1              : 1;
        volatile unsigned CCP2M2              : 1;
        volatile unsigned CCP2M3              : 1;
        volatile unsigned DC2B0               : 1;
        volatile unsigned DC2B1               : 1;
        volatile unsigned P2M0                : 1;
        volatile unsigned P2M1                : 1;
    };
    struct {
        volatile unsigned CCP2M               : 4;
        volatile unsigned DC2B                : 2;
        volatile unsigned P2M                 : 2;
    };
} CCP2CONbits @ 0x29A;
#endif

// Register: PWM2CON
volatile unsigned char           PWM2CON             @ 0x29B;
// bit and bitfield definitions
volatile bit P2DC0               @ ((unsigned)&PWM2CON*8)+0;
volatile bit P2DC1               @ ((unsigned)&PWM2CON*8)+1;
volatile bit P2DC2               @ ((unsigned)&PWM2CON*8)+2;
volatile bit P2DC3               @ ((unsigned)&PWM2CON*8)+3;
volatile bit P2DC4               @ ((unsigned)&PWM2CON*8)+4;
volatile bit P2DC5               @ ((unsigned)&PWM2CON*8)+5;
volatile bit P2DC6               @ ((unsigned)&PWM2CON*8)+6;
volatile bit P2RSEN              @ ((unsigned)&PWM2CON*8)+7;
#ifndef _LIB_BUILD
union {
    struct {
        volatile unsigned P2DC0               : 1;
        volatile unsigned P2DC1               : 1;
        volatile unsigned P2DC2               : 1;
        volatile unsigned P2DC3               : 1;
        volatile unsigned P2DC4               : 1;
        volatile unsigned P2DC5               : 1;
        volatile unsigned P2DC6               : 1;
        volatile unsigned P2RSEN              : 1;
    };
    struct {
        volatile unsigned P2DC                : 7;
    };
} PWM2CONbits @ 0x29B;
#endif

// Register: CCP2AS
volatile unsigned char           CCP2AS              @ 0x29C;
volatile unsigned char           ECCP2AS             @ 0x29C;
// bit and bitfield definitions
volatile bit PSS2BD0             @ ((unsigned)&CCP2AS*8)+0;
volatile bit PSS2BD1             @ ((unsigned)&CCP2AS*8)+1;
volatile bit PSS2AC0             @ ((unsigned)&CCP2AS*8)+2;
volatile bit PSS2AC1             @ ((unsigned)&CCP2AS*8)+3;
volatile bit CCP2AS0             @ ((unsigned)&CCP2AS*8)+4;
volatile bit CCP2AS1             @ ((unsigned)&CCP2AS*8)+5;
volatile bit CCP2AS2             @ ((unsigned)&CCP2AS*8)+6;
volatile bit CCP2ASE             @ ((unsigned)&CCP2AS*8)+7;
#ifndef _LIB_BUILD
union {
    struct {
        volatile unsigned PSS2BD0             : 1;
        volatile unsigned PSS2BD1             : 1;
        volatile unsigned PSS2AC0             : 1;
        volatile unsigned PSS2AC1             : 1;
        volatile unsigned CCP2AS0             : 1;
        volatile unsigned CCP2AS1             : 1;
        volatile unsigned CCP2AS2             : 1;
        volatile unsigned CCP2ASE             : 1;
    };
    struct {
        volatile unsigned PSS2BD              : 2;
        volatile unsigned PSS2AC              : 2;
        volatile unsigned CCP2AS              : 3;
    };
} CCP2ASbits @ 0x29C;
#endif

// Register: PSTR2CON
volatile unsigned char           PSTR2CON            @ 0x29D;
// bit and bitfield definitions
volatile bit STR2A               @ ((unsigned)&PSTR2CON*8)+0;
volatile bit STR2B               @ ((unsigned)&PSTR2CON*8)+1;
volatile bit STR2C               @ ((unsigned)&PSTR2CON*8)+2;
volatile bit STR2D               @ ((unsigned)&PSTR2CON*8)+3;
volatile bit STR2SYNC            @ ((unsigned)&PSTR2CON*8)+4;
#ifndef _LIB_BUILD
union {
    struct {
        volatile unsigned STR2A               : 1;
        volatile unsigned STR2B               : 1;
        volatile unsigned STR2C               : 1;
        volatile unsigned STR2D               : 1;
        volatile unsigned STR2SYNC            : 1;
    };
} PSTR2CONbits @ 0x29D;
#endif

// Register: CCPTMRS
volatile unsigned char           CCPTMRS             @ 0x29E;
volatile unsigned char           CCPTMRS0            @ 0x29E;
// bit and bitfield definitions
volatile bit C1TSEL0             @ ((unsigned)&CCPTMRS*8)+0;
volatile bit C1TSEL1             @ ((unsigned)&CCPTMRS*8)+1;
volatile bit C2TSEL0             @ ((unsigned)&CCPTMRS*8)+2;
volatile bit C2TSEL1             @ ((unsigned)&CCPTMRS*8)+3;
volatile bit C3TSEL0             @ ((unsigned)&CCPTMRS*8)+4;
volatile bit C3TSEL1             @ ((unsigned)&CCPTMRS*8)+5;
volatile bit C4TSEL0             @ ((unsigned)&CCPTMRS*8)+6;
volatile bit C4TSEL1             @ ((unsigned)&CCPTMRS*8)+7;
#ifndef _LIB_BUILD
union {
    struct {
        volatile unsigned C1TSEL0             : 1;
        volatile unsigned C1TSEL1             : 1;
        volatile unsigned C2TSEL0             : 1;
        volatile unsigned C2TSEL1             : 1;
        volatile unsigned C3TSEL0             : 1;
        volatile unsigned C3TSEL1             : 1;
        volatile unsigned C4TSEL0             : 1;
        volatile unsigned C4TSEL1             : 1;
    };
    struct {
        volatile unsigned C1TSEL              : 2;
        volatile unsigned C2TSEL              : 2;
        volatile unsigned C3TSEL              : 2;
        volatile unsigned C4TSEL              : 2;
    };
} CCPTMRSbits @ 0x29E;
#endif

//
// Special function register definitions: Bank 6
//

// Register: CCPR3L
volatile unsigned char           CCPR3L              @ 0x311;
// bit and bitfield definitions

// Register: CCPR3H
volatile unsigned char           CCPR3H              @ 0x312;
// bit and bitfield definitions

// Register: CCP3CON
volatile unsigned char           CCP3CON             @ 0x313;
// bit and bitfield definitions
volatile bit CCP3M0              @ ((unsigned)&CCP3CON*8)+0;
volatile bit CCP3M1              @ ((unsigned)&CCP3CON*8)+1;
volatile bit CCP3M2              @ ((unsigned)&CCP3CON*8)+2;
volatile bit CCP3M3              @ ((unsigned)&CCP3CON*8)+3;
volatile bit DC3B0               @ ((unsigned)&CCP3CON*8)+4;
volatile bit DC3B1               @ ((unsigned)&CCP3CON*8)+5;
#ifndef _LIB_BUILD
union {
    struct {
        volatile unsigned CCP3M0              : 1;
        volatile unsigned CCP3M1              : 1;
        volatile unsigned CCP3M2              : 1;
        volatile unsigned CCP3M3              : 1;
        volatile unsigned DC3B0               : 1;
        volatile unsigned DC3B1               : 1;
        volatile unsigned                     : 1;
        volatile unsigned                     : 1;
    };
    struct {
        volatile unsigned CCP3M               : 4;
        volatile unsigned DC3B                : 2;
    };
} CCP3CONbits @ 0x313;
#endif

// Register: CCPR4L
volatile unsigned char           CCPR4L              @ 0x318;
// bit and bitfield definitions

// Register: CCPR4H
volatile unsigned char           CCPR4H              @ 0x319;
// bit and bitfield definitions

// Register: CCP4CON
volatile unsigned char           CCP4CON             @ 0x31A;
// bit and bitfield definitions
volatile bit CCP4M0              @ ((unsigned)&CCP4CON*8)+0;
volatile bit CCP4M1              @ ((unsigned)&CCP4CON*8)+1;
volatile bit CCP4M2              @ ((unsigned)&CCP4CON*8)+2;
volatile bit CCP4M3              @ ((unsigned)&CCP4CON*8)+3;
volatile bit DC4B0               @ ((unsigned)&CCP4CON*8)+4;
volatile bit DC4B1               @ ((unsigned)&CCP4CON*8)+5;
#ifndef _LIB_BUILD
union {
    struct {
        volatile unsigned CCP4M0              : 1;
        volatile unsigned CCP4M1              : 1;
        volatile unsigned CCP4M2              : 1;
        volatile unsigned CCP4M3              : 1;
        volatile unsigned DC4B0               : 1;
        volatile unsigned DC4B1               : 1;
        volatile unsigned                     : 2;
    };
    struct {
        volatile unsigned CCP4M               : 4;
        volatile unsigned DC4B                : 2;
    };
} CCP4CONbits @ 0x31A;
#endif

//
// Special function register definitions: Bank 7
//

// Register: IOCBP
// Interrupt-On-Change Positive Edge Register
volatile unsigned char           IOCBP               @ 0x394;
// bit and bitfield definitions
volatile bit IOCBP0              @ ((unsigned)&IOCBP*8)+0;
volatile bit IOCBP1              @ ((unsigned)&IOCBP*8)+1;
volatile bit IOCBP2              @ ((unsigned)&IOCBP*8)+2;
volatile bit IOCBP3              @ ((unsigned)&IOCBP*8)+3;
volatile bit IOCBP4              @ ((unsigned)&IOCBP*8)+4;
volatile bit IOCBP5              @ ((unsigned)&IOCBP*8)+5;
volatile bit IOCBP6              @ ((unsigned)&IOCBP*8)+6;
volatile bit IOCBP7              @ ((unsigned)&IOCBP*8)+7;
#ifndef _LIB_BUILD
union {
    struct {
        volatile unsigned IOCBP0              : 1;
        volatile unsigned IOCBP1              : 1;
        volatile unsigned IOCBP2              : 1;
        volatile unsigned IOCBP3              : 1;
        volatile unsigned IOCBP4              : 1;
        volatile unsigned IOCBP5              : 1;
        volatile unsigned IOCBP6              : 1;
        volatile unsigned IOCBP7              : 1;
    };
} IOCBPbits @ 0x394;
#endif

// Register: IOCBN
// Interrupt-On-Change Negative Edge Register
volatile unsigned char           IOCBN               @ 0x395;
// bit and bitfield definitions
volatile bit IOCBN0              @ ((unsigned)&IOCBN*8)+0;
volatile bit IOCBN1              @ ((unsigned)&IOCBN*8)+1;
volatile bit IOCBN2              @ ((unsigned)&IOCBN*8)+2;
volatile bit IOCBN3              @ ((unsigned)&IOCBN*8)+3;
volatile bit IOCBN4              @ ((unsigned)&IOCBN*8)+4;
volatile bit IOCBN5              @ ((unsigned)&IOCBN*8)+5;
volatile bit IOCBN6              @ ((unsigned)&IOCBN*8)+6;
volatile bit IOCBN7              @ ((unsigned)&IOCBN*8)+7;
#ifndef _LIB_BUILD
union {
    struct {
        volatile unsigned IOCBN0              : 1;
        volatile unsigned IOCBN1              : 1;
        volatile unsigned IOCBN2              : 1;
        volatile unsigned IOCBN3              : 1;
        volatile unsigned IOCBN4              : 1;
        volatile unsigned IOCBN5              : 1;
        volatile unsigned IOCBN6              : 1;
        volatile unsigned IOCBN7              : 1;
    };
} IOCBNbits @ 0x395;
#endif

// Register: IOCBF
// Interrupt-On-Change Flag Register
volatile unsigned char           IOCBF               @ 0x396;
// bit and bitfield definitions
volatile bit IOCBF0              @ ((unsigned)&IOCBF*8)+0;
volatile bit IOCBF1              @ ((unsigned)&IOCBF*8)+1;
volatile bit IOCBF2              @ ((unsigned)&IOCBF*8)+2;
volatile bit IOCBF3              @ ((unsigned)&IOCBF*8)+3;
volatile bit IOCBF4              @ ((unsigned)&IOCBF*8)+4;
volatile bit IOCBF5              @ ((unsigned)&IOCBF*8)+5;
volatile bit IOCBF6              @ ((unsigned)&IOCBF*8)+6;
volatile bit IOCBF7              @ ((unsigned)&IOCBF*8)+7;
#ifndef _LIB_BUILD
union {
    struct {
        volatile unsigned IOCBF0              : 1;
        volatile unsigned IOCBF1              : 1;
        volatile unsigned IOCBF2              : 1;
        volatile unsigned IOCBF3              : 1;
        volatile unsigned IOCBF4              : 1;
        volatile unsigned IOCBF5              : 1;
        volatile unsigned IOCBF6              : 1;
        volatile unsigned IOCBF7              : 1;
    };
} IOCBFbits @ 0x396;
#endif

// Register: CLKRCON
volatile unsigned char           CLKRCON             @ 0x39A;
// bit and bitfield definitions
volatile bit CLKRDIV0            @ ((unsigned)&CLKRCON*8)+0;
volatile bit CLKRDIV1            @ ((unsigned)&CLKRCON*8)+1;
volatile bit CLKRDIV2            @ ((unsigned)&CLKRCON*8)+2;
volatile bit CLKRDC0             @ ((unsigned)&CLKRCON*8)+3;
volatile bit CLKRDC1             @ ((unsigned)&CLKRCON*8)+4;
volatile bit CLKRSLR             @ ((unsigned)&CLKRCON*8)+5;
volatile bit CLKROE              @ ((unsigned)&CLKRCON*8)+6;
volatile bit CLKREN              @ ((unsigned)&CLKRCON*8)+7;
#ifndef _LIB_BUILD
union {
    struct {
        volatile unsigned CLKRDIV0            : 1;
        volatile unsigned CLKRDIV1            : 1;
        volatile unsigned CLKRDIV2            : 1;
        volatile unsigned CLKRDC0             : 1;
        volatile unsigned CLKRDC1             : 1;
        volatile unsigned CLKRSLR             : 1;
        volatile unsigned CLKROE              : 1;
        volatile unsigned CLKREN              : 1;
    };
    struct {
        volatile unsigned CLKRDIV             : 3;
        volatile unsigned CLKRDC              : 2;
    };
} CLKRCONbits @ 0x39A;
#endif

// Register: MDCON
volatile unsigned char           MDCON               @ 0x39C;
// bit and bitfield definitions
volatile bit MDBIT               @ ((unsigned)&MDCON*8)+0;
volatile bit MDOPOL              @ ((unsigned)&MDCON*8)+4;
volatile bit MDSLR               @ ((unsigned)&MDCON*8)+5;
volatile bit MDOE                @ ((unsigned)&MDCON*8)+6;
volatile bit MDEN                @ ((unsigned)&MDCON*8)+7;
#ifndef _LIB_BUILD
union {
    struct {
        volatile unsigned MDBIT               : 1;
        volatile unsigned                     : 3;
        volatile unsigned MDOPOL              : 1;
        volatile unsigned MDSLR               : 1;
        volatile unsigned MDOE                : 1;
        volatile unsigned MDEN                : 1;
    };
} MDCONbits @ 0x39C;
#endif

// Register: MDSRC
volatile unsigned char           MDSRC               @ 0x39D;
// bit and bitfield definitions
volatile bit MDMS0               @ ((unsigned)&MDSRC*8)+0;
volatile bit MDMS1               @ ((unsigned)&MDSRC*8)+1;
volatile bit MDMS2               @ ((unsigned)&MDSRC*8)+2;
volatile bit MDMS3               @ ((unsigned)&MDSRC*8)+3;
volatile bit MDMSODIS            @ ((unsigned)&MDSRC*8)+7;
#ifndef _LIB_BUILD
union {
    struct {
        volatile unsigned MDMS0               : 1;
        volatile unsigned MDMS1               : 1;
        volatile unsigned MDMS2               : 1;
        volatile unsigned MDMS3               : 1;
        volatile unsigned                     : 3;
        volatile unsigned MDMSODIS            : 1;
    };
    struct {
        volatile unsigned MDMS                : 4;
    };
} MDSRCbits @ 0x39D;
#endif

// Register: MDCARL
volatile unsigned char           MDCARL              @ 0x39E;
// bit and bitfield definitions
volatile bit MDCL0L               @ ((unsigned)&MDCARL*8)+0;
volatile bit MDCL1L               @ ((unsigned)&MDCARL*8)+1;
volatile bit MDCL2L               @ ((unsigned)&MDCARL*8)+2;
volatile bit MDCL3L               @ ((unsigned)&MDCARL*8)+3;
volatile bit MDCLSYNCL            @ ((unsigned)&MDCARL*8)+5;
volatile bit MDCLPOLL             @ ((unsigned)&MDCARL*8)+6;
volatile bit MDCLODISL            @ ((unsigned)&MDCARL*8)+7;
#ifndef _LIB_BUILD
union {
    struct {
        volatile unsigned MDCL0               : 1;
        volatile unsigned MDCL1               : 1;
        volatile unsigned MDCL2               : 1;
        volatile unsigned MDCL3               : 1;
        volatile unsigned                     : 1;
        volatile unsigned MDCLSYNC            : 1;
        volatile unsigned MDCLPOL             : 1;
        volatile unsigned MDCLODIS            : 1;
    };
    struct {
        volatile unsigned MDCL                : 4;
    };
} MDCARLbits @ 0x39E;
#endif

// Register: MDCARH
volatile unsigned char           MDCARH              @ 0x39F;
// bit and bitfield definitions
volatile bit MDCL0H               @ ((unsigned)&MDCARH*8)+0;
volatile bit MDCL1H               @ ((unsigned)&MDCARH*8)+1;
volatile bit MDCL2H               @ ((unsigned)&MDCARH*8)+2;
volatile bit MDCL3H               @ ((unsigned)&MDCARH*8)+3;
volatile bit MDCLSYNCH            @ ((unsigned)&MDCARH*8)+5;
volatile bit MDCLPOLH             @ ((unsigned)&MDCARH*8)+6;
volatile bit MDCLODISH            @ ((unsigned)&MDCARH*8)+7;
#ifndef _LIB_BUILD
union {
    struct {
        volatile unsigned MDCL0               : 1;
        volatile unsigned MDCL1               : 1;
        volatile unsigned MDCL2               : 1;
        volatile unsigned MDCL3               : 1;
        volatile unsigned                     : 1;
        volatile unsigned MDCLSYNC            : 1;
        volatile unsigned MDCLPOL             : 1;
        volatile unsigned MDCLODIS            : 1;
    };
    struct {
        volatile unsigned MDCL                : 4;
    };
} MDCARHbits @ 0x39F;
#endif

//
// Special function register definitions: Bank 8
//

// Register: TMR4
volatile unsigned char           TMR4                @ 0x415;
// bit and bitfield definitions

// Register: PR4
volatile unsigned char           PR4                 @ 0x416;
// bit and bitfield definitions

// Register: T4CON
// Timer2 Control Register
volatile unsigned char           T4CON               @ 0x417;
// bit and bitfield definitions
// Timer2 Clock Prescale Select bits
volatile bit T4CKPS0             @ ((unsigned)&T4CON*8)+0;
// Timer2 Clock Prescale Select bits
volatile bit T4CKPS1             @ ((unsigned)&T4CON*8)+1;
// Timer2 On bit
volatile bit TMR4ON              @ ((unsigned)&T4CON*8)+2;
// Timer2 Output Postscaler Select bits
volatile bit T4OUTPS0            @ ((unsigned)&T4CON*8)+3;
// Timer2 Output Postscaler Select bits
volatile bit T4OUTPS1            @ ((unsigned)&T4CON*8)+4;
// Timer2 Output Postscaler Select bits
volatile bit T4OUTPS2            @ ((unsigned)&T4CON*8)+5;
// Timer2 Output Postscaler Select bits
volatile bit T4OUTPS3            @ ((unsigned)&T4CON*8)+6;
#ifndef _LIB_BUILD
union {
    struct {
        volatile unsigned T4CKPS0             : 1;
        volatile unsigned T4CKPS1             : 1;
        volatile unsigned TMR4ON              : 1;
        volatile unsigned T4OUTPS0            : 1;
        volatile unsigned T4OUTPS1            : 1;
        volatile unsigned T4OUTPS2            : 1;
        volatile unsigned T4OUTPS3            : 1;
        volatile unsigned                     : 1;
    };
    struct {
        volatile unsigned T4CKPS              : 2;
        volatile unsigned                     : 1;
        volatile unsigned T4OUTPS             : 4;
    };
} T4CONbits @ 0x417;
#endif

// Register: TMR6
volatile unsigned char           TMR6                @ 0x41C;
// bit and bitfield definitions

// Register: PR6
volatile unsigned char           PR6                 @ 0x41D;
// bit and bitfield definitions

// Register: T6CON
// Timer2 Control Register
volatile unsigned char           T6CON               @ 0x41E;
// bit and bitfield definitions
// Timer2 Clock Prescale Select bits
volatile bit T6CKPS0             @ ((unsigned)&T6CON*8)+0;
// Timer2 Clock Prescale Select bits
volatile bit T6CKPS1             @ ((unsigned)&T6CON*8)+1;
// Timer2 On bit
volatile bit TMR6ON              @ ((unsigned)&T6CON*8)+2;
// Timer2 Output Postscaler Select bits
volatile bit T6OUTPS0            @ ((unsigned)&T6CON*8)+3;
// Timer2 Output Postscaler Select bits
volatile bit T6OUTPS1            @ ((unsigned)&T6CON*8)+4;
// Timer2 Output Postscaler Select bits
volatile bit T6OUTPS2            @ ((unsigned)&T6CON*8)+5;
// Timer2 Output Postscaler Select bits
volatile bit T6OUTPS3            @ ((unsigned)&T6CON*8)+6;
#ifndef _LIB_BUILD
union {
    struct {
        volatile unsigned T6CKPS0             : 1;
        volatile unsigned T6CKPS1             : 1;
        volatile unsigned TMR6ON              : 1;
        volatile unsigned T6OUTPS0            : 1;
        volatile unsigned T6OUTPS1            : 1;
        volatile unsigned T6OUTPS2            : 1;
        volatile unsigned T6OUTPS3            : 1;
        volatile unsigned                     : 1;
    };
    struct {
        volatile unsigned T6CKPS              : 2;
        volatile unsigned                     : 1;
        volatile unsigned T6OUTPS             : 4;
    };
} T6CONbits @ 0x41E;
#endif

//
// Special function register definitions: Bank 31
//

// Register: STATUS_SHAD
volatile unsigned char           STATUS_SHAD         @ 0xFE4;
// bit and bitfield definitions
volatile bit C_SHAD              @ ((unsigned)&STATUS_SHAD*8)+0;
volatile bit DC_SHAD             @ ((unsigned)&STATUS_SHAD*8)+1;
volatile bit Z_SHAD              @ ((unsigned)&STATUS_SHAD*8)+2;
#ifndef _LIB_BUILD
union {
    struct {
        volatile unsigned C_SHAD              : 1;
        volatile unsigned DC_SHAD             : 1;
        volatile unsigned Z_SHAD              : 1;
    };
} STATUS_SHADbits @ 0xFE4;
#endif

// Register: WREG_SHAD
volatile unsigned char           WREG_SHAD           @ 0xFE5;
// bit and bitfield definitions

// Register: BSR_SHAD
volatile unsigned char           BSR_SHAD            @ 0xFE6;
// bit and bitfield definitions
#ifndef _LIB_BUILD
union {
    struct {
        volatile unsigned BSR_SHAD            : 5;
    };
} BSR_SHADbits @ 0xFE6;
#endif

// Register: PCLATH_SHAD
volatile unsigned char           PCLATH_SHAD         @ 0xFE7;
// bit and bitfield definitions
#ifndef _LIB_BUILD
union {
    struct {
        volatile unsigned PCLATH_SHAD         : 7;
    };
} PCLATH_SHADbits @ 0xFE7;
#endif

// Register: FSR0L_SHAD
volatile unsigned char           FSR0L_SHAD          @ 0xFE8;
// bit and bitfield definitions

// Register: FSR0H_SHAD
volatile unsigned char           FSR0H_SHAD          @ 0xFE9;
// bit and bitfield definitions

// Register: FSR1L_SHAD
volatile unsigned char           FSR1L_SHAD          @ 0xFEA;
// bit and bitfield definitions

// Register: FSR1H_SHAD
volatile unsigned char           FSR1H_SHAD          @ 0xFEB;
// bit and bitfield definitions

// Register: STKPTR
// Current Stack pointer
volatile unsigned char           STKPTR              @ 0xFED;
// bit and bitfield definitions
#ifndef _LIB_BUILD
union {
    struct {
        volatile unsigned STKPTR              : 5;
    };
} STKPTRbits @ 0xFED;
#endif

// Register: TOSL
// Top of Stack Low byte
volatile unsigned char           TOSL                @ 0xFEE;
// bit and bitfield definitions

// Register: TOSH
// Top of Stack High byte
volatile unsigned char           TOSH                @ 0xFEF;
// bit and bitfield definitions
#ifndef _LIB_BUILD
union {
    struct {
        volatile unsigned TOSH                : 7;
    };
} TOSHbits @ 0xFEF;
#endif

#endif
