//	**************************************************************************************
//	
//	Project Name: Alvara� Radio Modem Control Interface
//	
//	File Name: RMCI_main.c
//	
//	Release:	C10 - FW01 - 2.1.1
//	
//	Author:	F. Agnello
//	
//	**************************************************************************************
//	
//	� Ceravision Ltd - 2012
//	
//	*******************************************
//
//	REVISION HISTORY
//	--------------------------------------------------------------------------
//
//	* RELEASE 2.1.0 *
//
//	1. Added the option to put the device in command mode
//	by setting the input RA0 to low
//
//	2. Added the option to reset the device by software. When in command mode
// 	the function code RESET_DEVICE will reset the device.
//
//	3. Bootloading a new program code is now allowed by using the 
//	reset command described above. Bootloading is also possible at the 
//	power on reset.
//
//	4. Added new error codes to make troubleshooting easier:
//
//	- FC_NACK = function code not recognize
//	- FC_CAST_ERROR = error in casting message
//	- RM_NACK_ERROR = Radio Modem messaging not acknowledge
//	- RM_TIMEOUT_ERROR = RadioModem messaging time out
//
//	6. The program code has been optimize in order to be allocatable in the
//	program memory along whit the bootloader code. The execution of the read/write
//	function code became a function returning one of the following code:
//
//	-  NO_ERROR
//	- CHECKSUM_ERROR
//	- MD_NACK_ERROR
//	- MD_TIMEOUT_ERROR
//	- FC_NACK
//	- FC_CAST_ERROR
//	- RM_NACK_ERROR
//	- RM_TIMEOUT_ERROR
//		
//	7. The function code Get_Unid_ID have been removed because
//	meaningless.
//
//	8. Software parameter setting is accomplished using the new
//	procedure SW_Unit_Config
//
//	9. At the power on reset the channel of the radio modem device 
//	is set accordingly to what is written in the eeprom memory
//	at EEPROM_RF_CHANNEL
//
//	* RELEASE 2.1.1 *
//
//	10. Added a difference between generation 1 and 2 of ALVARA when compiling and
//	when reading the firnware ID. ALAVARA_GEN is used to make a difference when compiling
//	and the same when returning the firmware ID.
//


// 
// !! IMPORTANT !!
//
// DEFINE HERE WHICH GENERATION OF ALVARA TO COMPILE FOR: 0 = Gen1(md400) ; 1 = Gen2(mdquad)
// ALVARA_GEN is used from the compiler to know which constants/variables to use
//
#define ALVARA_GEN 0

//	INCLUDE
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#include <stdio.h>
#include <string.h>
#include<pic.h>
#include<htc.h>
#include"RMCI_header_cnst.h"
#include"RMCI_header_vrbl.h"


//	Firmware release
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
const char *FirmwareID = "C10FW01211_G";


//	CONFIGURATION BITS
////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Program config. word 1

// watchdog enabled
//After a Reset, the default time-out period is 2 seconds
__CONFIG(FOSC_INTOSC & WDTE_ON & PWRTE_OFF 
			& MCLRE_ON & CP_OFF & CPD_OFF & BOREN_OFF 
				& CLKOUTEN_OFF & IESO_ON & FCMEN_ON);

// Program config. word 2
//__CONFIG(WRT_OFF & PLLEN_ON & STVREN_ON
//			& BORV_19 & LVP_OFF);
__CONFIG(WRT_OFF & PLLEN_ON & STVREN_ON
			& BORV_LO & LVP_OFF);


//	FUNCTION PROTOTYPE
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void HW_Unit_Config(void);
void SW_Unit_Config(void);

unsigned char Execute_FC(unsigned char Function_Code, unsigned char Dummy_ByteCount, unsigned char Inbound_Broadcast );

void Write_EEProm_Byte(unsigned char address, unsigned char data);
void SwitchBuffer(unsigned char Mode);
void GetUnitStatus(unsigned char DataCount);
void SendMessage(unsigned char Error_Code);

unsigned int Send_PowerSupply_Command(unsigned char Mode, unsigned char RegAdd,unsigned char RegData);
unsigned short getCRC16(unsigned char *s, unsigned char lenght);
unsigned char READ_EEPROM_BYTE(unsigned char address);
unsigned char ReadModemChannel(unsigned char *pData);
unsigned char WriteModemChannel(unsigned char NewChannel);
unsigned char Verify_Incoming_Message(unsigned char *pBuffer, unsigned char DataLength);
unsigned char Send_AT_Command( const unsigned char *c);


//	INTERRUPT SERVICE ROUTINE
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// The function qualifier "interrupt" may be applied to C function definitions to allow them to be
// called directly from the hardware interrupts. The compiler will process the interrupt function
// differently to any other functions, generating code to save and restore any registers used and exit
// using the appropriate instruction.
//
void interrupt isr(void) { //@ 0x10
 
	static unsigned char isr_dummy;  

//
// Receiver_ISR

	if(RCIE==1 && PIR1bits.RCIF==1)
	{ 
		PIR1bits.RCIF = 0;
		RCIE=0;  
	
		// check if Rx error
		if(RCSTAbits.OERR==1)
		{
			RCSTAbits.CREN=0;
			RCSTAbits.CREN=1;
		}
		else if (RCSTAbits.FERR==1)
		{
			isr_dummy = RCREG;  
		}
		else // if no error then save the received byte
		{
			switch (BufferMode)
			{
				case MAGDRIVE:
				{
					// save data in the buffer
					IntBuffer[IntBufferPos]=RCREG;
	
					// update buffer pointer
					if (++IntBufferPos==INT_BUFFER_DIM){IntBufferPos=0;}
	
					// check message length
					if ((++IntCharCount) == MD_OUTBOUND_MESSAGE_LEN)
					{ // the buffer is full
						InBoundMessage = TRUE;
						// reset buffer pointer
						IntBufferPos = 0;
					}
	
					break;
				}
				case TRANSCEIVER:
				case CMD_MODE:
				{
					// save data in the buffer
					ExtBuffer[ExtBufferPos]=RCREG;
					// update buffer pointer
					if (++ExtBufferPos==EXT_BUFFER_DIM){ExtBufferPos=0;}
					// enable idle check timer
					RxIdleCheck=1;
					break;
				} 
			
			} // end switch BufferMode
	
		}
	
		RCIE=1;
		
	}// end Receiver_ISR


//
// TMR0_ISR (every 1 msec @ 4MHz)

	if (TMR0IE==1 && INTCONbits.TMR0IF==1)
	{ 
			
		// Update Counters
		TimeOut_Timer -- ;
		ScanMD_TimeOut ++;
		CmdMode_TimeOut_Timer++;
	
		Bootstrap_TimeOut_Timer++;
	
		// Update inbound message flag status 
		if (!TimeOut_Timer)
		{
			RxModemTimeOut = InBoundMessage_TimeOut = TRUE;
		}
	
	 	// Raise CmdMode_TimeOut flag
		if(CmdMode_TimeOut_Timer > CMD_MODE_TIMEOUT)
		{
			Cmd_Mode_Sw = FALSE;
		}
	
		if (Bootstrap_TimeOut_Timer > BOOTSTRAP_TIMEOUT)
		{
			Bootstrap_TimeOut = TRUE;
		}
	
		// Update Scan Magdrive timer related flag status 
		if((ScanMD_TimeOut > MD_BEACON_TIMEOUT) && Master_OnLine)
		{
		
			// Reset timer
			ScanMD_TimeOut = 0;        
			// set "scan magdrive" conditon
			ScanMagDrive = TRUE;
		
		} // end Update timer related flag status
	
		// Update the master offline timeout counter
		if (!(--Master_Inner_TimeOut_Counter))
		{
		
			Master_Inner_TimeOut_Counter = MASTER_TIMEOUT_RELOAD;
			if (! (-- Master_Outer_TimeOut_Counter ))
			{
			
				// Reset timer
				Master_Outer_TimeOut_Counter  = Master_OffLine_TimeOut_Reload;        
				// set the master offline conditon
				Release_Alvara = Master_OnLine;
				Master_OnLine = false;
			
			} // end Update timer related flag status
		
		}
		
		// INCOMING MESSAGE TIME-OUT (RX LINE IDLE STATE)
		//
		// RxIdleCheck is set to 1 each time a new incoming byte is expected.
		// When RxIdleCheck becomes higher than RX_IDLE_TIMEOUT
		// the last byte in a message has been received and
		// a new message is ready to be read.
		//
		if(( RxIdleCheck) && (++RxIdleCheck>RX_IDLE_TIMEOUT))
		{
				// disable idle check timer
				RxIdleCheck=0;
	
				if(ExtBufferPos)
				{ // the buffer is full
							
					InBoundModemMessage=TRUE;
					FrameLength=ExtBufferPos;
					// reset buffer pointer
					ExtBufferPos=0;
				
				}
		 
		} // end check for incomimng message timeout
	
	
		// clear TMR0 Interrupt Flag          
		INTCONbits.TMR0IF = 0;
		    
		// reset Timer0 counter value (1 msec @ 4MHz)
		TMR0 = TMRO_START_VALUE;
	
	} // end TMR0_ISR

}//  end interrupt service routine code


 
//	FUNCTION & PROCEDURE
////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**************************************************************************
 *  
 *  Send_AT_Command
 *  
 * This function sends an AT command to the radio modem device. The string is
 * given by the caller as the pointer to the string, and the fucntion returns one of the
 * folowings codes:
 *
 *   Returned value:
 *	- NO_ERROR
 *	- RM_TIMEOUT_ERROR
 *
 ***************************************************************************/
unsigned char Send_AT_Command(const unsigned char  *String)
{ 

const unsigned char  *s1;

	s1 = String;
	
	while (*(String)!='\0') 
	{ 
		TXREG = *(String); 
	 	while (!TXSTAbits.TRMT);  
		String ++;
	} 

	if (*s1 != '+')
	{
		TXREG = 0x0D; //Carriage return 
		while (!TXSTAbits.TRMT); 
		TXREG = 0x0A; //Line Feed 
		while (!TXSTAbits.TRMT);
	}

	// Set up time out timer
	TimeOut_Timer = RM_OUTBOUND_TIMEOUT;
   	InBoundModemMessage = false;
   	RxModemTimeOut = false;

   	while (!(InBoundModemMessage||RxModemTimeOut));

	// Exit when time out
	if (RxModemTimeOut)
		return RM_TIMEOUT_ERROR;

	return NO_ERROR;

}// end of Send_AT_Command


/**************************************************************************
 *  
 *  ReadModemChannel
 *  
 * This function interacts with the radio modem device by asking what's the contents of S2 register
 * which holds the value of the working channel. If no errors occur during the process, the valid
 *  information will be available in ExtBuffer[0].
 *
 * Send_AT_Command returns no_error if somenthing is received back from
 * the modem but the meaning of the answer must be evaluated from the calling
 * function. Otherwise, the time out error code is returned.
 *
 *   Returned value:
 *	- NO ERROR
 *	- RM_NACK_ERROR
 *	- RM_TIMEOUT_ERROR
 *
 ***************************************************************************/
unsigned char ReadModemChannel(unsigned char *pData){

	unsigned char RM_Comm_Error;
	
	RM_Comm_Error = Send_AT_Command("+++");
	
	if (!(RM_Comm_Error)){
		
		// check the answer if not "OK"
		if (strncmp(&ExtBuffer[0],"OK",2)){
			RM_Comm_Error = RM_NACK_ERROR;
		}
		else{
			RM_Comm_Error = Send_AT_Command("ATS2");
			if (!(RM_Comm_Error)){
				RM_Comm_Error = RM_NACK_ERROR;
				// check the answer if Ch = [0 .. 9]
				if((0x30 <= ExtBuffer[0]) && (ExtBuffer[0] <= 0x39)){
					(*pData) = ExtBuffer[0] -0x30;
					RM_Comm_Error = NO_ERROR;
				}	
			}
		}
			
		// Set the modem in data mode before return
		Send_AT_Command("ATCC");
	}
	
	return RM_Comm_Error;

}// end of ReadModemChannel



/**************************************************************************
 *  
 *  WriteModemChannel Function
 *  
 * This function interacts with the radio modem device by writing the of S2 register
 * which holds the value of the working channel.
 *
 * Send_AT_Command returns no_error if somenthing is received back from
 * the modem but the meaning of the answer must be evaluated from the calling
 * function. Otherwise, the time out error code is returned.
 *
 *   Returned value:
 *	- NO ERROR
 *	- RM_NACK_ERROR
 *	- RM_TIME_OUT
 *
 ***************************************************************************/
unsigned char WriteModemChannel(unsigned char NewChannel)
{

	unsigned char RM_Comm_Error;
	unsigned char WriteChannel[10];
	
	WriteChannel[0]='A';
	WriteChannel[1]='T';
	WriteChannel[2]='S';
	WriteChannel[3]='2';
	WriteChannel[4]='=';
	WriteChannel[5]= NewChannel + 0x30;
	WriteChannel[6]= ',';
	WriteChannel[7]= 'W';
	WriteChannel[8]= 'R';
	WriteChannel[9]='\0';
	
	RM_Comm_Error = Send_AT_Command("+++");
	if (!(RM_Comm_Error)){
		
		// check the answer if not "OK"
		if (strncmp(&ExtBuffer[0],"OK",2)){
			RM_Comm_Error = RM_NACK_ERROR;
		}
		else{
			RM_Comm_Error = Send_AT_Command(WriteChannel);
			if (!(RM_Comm_Error)){
				// check the answer if not "OK"
				if(strncmp(&ExtBuffer[0],"OK",2)){
					RM_Comm_Error = RM_NACK_ERROR;			
				}
			}
		}
		// Set the modem in data mode before return
		Send_AT_Command("ATCC");
	}
	
	return RM_Comm_Error;

}// end of WriteModemChannel


/**************************************************************************
 *  
 *  SwitchBuffer Procedure
 *  
 * This procedure swaps the three-state buffer between the internal and external buffer.
 * The current status is stored in the global variable BufferMode.
 *
 * The extra NOP instructions are aimed to guarantee what follows.
 * Three-state outputs are designed to be tied together but
 * are not intended to be active simultaneously. To minimize
 * noise and to protect outputs from excessive power
 * dissipation, only one three-state output should be active at
 * any time. In general, this requires that the output enable
 * signals should not overlap.
 * 
 ***************************************************************************/
void SwitchBuffer(unsigned char Mode)
{

	// Set new current status of the buffer output
	BufferMode = Mode;
	
	if (Mode == TRANSCEIVER){
	
	// Switch Buffer to the RF Transceiver
	
		// disable first buffer
		DISABLE_MAGDRIVE
		// output disable delay time
		asm("nop");
		asm("nop");
		// enable second buffer
		ENABLE_TRANSCEIVER
		
	}
	else if ((Mode == MAGDRIVE) || (Mode == CMD_MODE)) {
	
	// Switch Buffer to the Power Supply
	// MAGDRIVE and CMD_MODE are logically different but they imply the same
	// configuration of the three-state buffer.
	
		// disable first buffer
		DISABLE_TRANSCEIVER
		// output disable delay time
		asm("nop");
		asm("nop");
		// enable second buffer
		ENABLE_MAGDRIVE
		
	}
	
	// electronic switching delay
	 asm("nop");
	 asm("nop");

}// end of SwitchBuffer


/**************************************************************************
 *  
 *  Verify_Incoming_Message
 *  
 * This function checks if the inbound message is addressed to the unit either as a unicast, 
 * multicast or a broadcast message
 *
 ***************************************************************************/
unsigned char Verify_Incoming_Message(unsigned char *pBuffer, unsigned char DataLength){
	
	unsigned short CheckReceivedCRC;
	unsigned char Comm_Error;
	

	Comm_Error = NO_ERROR;
			
	CheckReceivedCRC = (*(pBuffer + DataLength - 2) <<8)|(*(pBuffer + DataLength - 1));
	CheckReceivedCRC -= getCRC16(pBuffer,DataLength-2);
	
	if (CheckReceivedCRC){
		Comm_Error = CHECKSUM_ERROR;
	}
	// Check if Broadcast	
	else if ((ExtBuffer[GROUP_ADDRESS] != UnitID.GroupAddress) && (ExtBuffer[GROUP_ADDRESS]  != BROADCAST)){
		//Group Address Error
		Comm_Error = NACK_ERROR;
	}
	// check if multicast
	else if ((ExtBuffer[UNIT_ADDRESS] != UnitID.UnitAddress) && (ExtBuffer[UNIT_ADDRESS] != MULTICAST)){
		// Unit Address Error
		Comm_Error = NACK_ERROR;
	 }
	
	if (Comm_Error != CHECKSUM_ERROR){
		
		// Reset master off-line timer
		Master_Inner_TimeOut_Counter = MASTER_TIMEOUT_RELOAD;
		Master_Outer_TimeOut_Counter = Master_OffLine_TimeOut_Reload;
		
		// set the master on line condition
		Master_OnLine = TRUE;
	}
		 		
	return Comm_Error;

}// end of VeirfyIncomingMessage


/**************************************************************************
 *  
 *  GetUnitStatus Procedure
 *  
 ***************************************************************************/
void GetUnitStatus(unsigned char DataCount){

	unsigned char Command_Error;
	unsigned char *Dummy_P1;
	unsigned short index;

	// set unit status Online = true
	UnitStatus.OnLine = true;

	// read MAX_NUMBER_RESTART
	Command_Error = Send_PowerSupply_Command(READ_EEPROM,MAX_NUMBER_RESTART,0);
	if (!(Command_Error)){
		UnitStatus.MaxRestartNumber = IntBuffer[1];
	}
	else if (Command_Error == MD_TIMEOUT_ERROR){
		UnitStatus.OnLine = FALSE;
	}
	
	// read NUMBER_OF_RESTART
	Command_Error = Send_PowerSupply_Command(READ_EEPROM,NUMBER_OF_RESTART,0);
	if (!(Command_Error)){
		UnitStatus.NumberRestart = IntBuffer[1];
	}
	else if (Command_Error == MD_TIMEOUT_ERROR){
		UnitStatus.OnLine = FALSE;
	}
	
	// read ALARMS
	Command_Error =  Send_PowerSupply_Command(READ_EEPROM,ALARMS,0);	
	if (!(Command_Error)){
		UnitStatus.Alarms = IntBuffer[1];
	}
	else if (Command_Error == MD_TIMEOUT_ERROR){
		UnitStatus.OnLine = FALSE;
	}
	
	// read POWER_SETTING
	Command_Error = Send_PowerSupply_Command(READ_RAM,POWER_SETTING,0);
	if (!(Command_Error)){
		UnitStatus.PowerSetting = IntBuffer[1];
	}
	else if (Command_Error == MD_TIMEOUT_ERROR){
		UnitStatus.OnLine = FALSE;
	}
	
	// read ELECTRICAL_POWER
	Command_Error = Send_PowerSupply_Command(READ_RAM,ELECTRICAL_POWER,0);
	if (!(Command_Error)){
		UnitStatus.PowerReading = IntBuffer[1];
	}
	else if (Command_Error == MD_TIMEOUT_ERROR){
		UnitStatus.OnLine = FALSE;
	}

	if (DataCount){
		
		Dummy_P1 = &UnitStatus.MaxRestartNumber;
		for(index=0; index < PS_STATUS_LEN; index++)
		{
			OutModemBuffer[DataCount] = *(Dummy_P1);
			Dummy_P1 ++ ;
			DataCount ++ ;
		}
			
		Dummy_P1 = &SceneList.Scene_0;
		for(index=0; index < LIGHTING_SCENE_NUMBER; index++)
		{
			OutModemBuffer[DataCount] = *(Dummy_P1);
			Dummy_P1 ++ ;
			DataCount ++ ;
		}
	}	

} // end of GetUnitStatus


/****************************************************************************************************
 *  
 *  Send_PowerSupply_Command Function
 *  
 *   Input:
 *    Mode - MagDrive400 function code: READ_RAM, READ_EEPROM, WRITE_RAM, WRITE_EEPROM
 *    RegAdd - MagDrive400 register address
 *    RegData  - Data value
 *
 *   Return: 
 *    - NO ERROR
 *    - CheckSum ERROR
 *    - MD_NACK_ERROR
 *    - MD_TIMEOUT_ERROR
 *
 ****************************************************************************************************/
unsigned int Send_PowerSupply_Command(unsigned char Mode, unsigned char RegAdd,unsigned char RegData){

	unsigned short i=0;
	unsigned char packet[4];
	unsigned short checksum;
	unsigned short PicMagDriveChecksum; 
	unsigned static char tryAgain;
	unsigned char Escape;
	unsigned char PositionData_Zero;
	
	// Reset control variable
	checksum=0;    
	PicMagDriveChecksum=0;
	tryAgain=0;
	
	// Switch the serial port to the INTernal buffer
	SwitchBuffer(MAGDRIVE);

	PositionData_Zero = 0;
	
	// Map the outgoing message
	packet[0] = Mode;
	packet[1] = RegAdd;
	PositionData_Zero += RegAdd;
	packet[2] = RegData;
	PositionData_Zero += RegData;
		
	// Add checksum
	checksum = (unsigned int)((unsigned int)(Mode) + (unsigned int)(RegAdd) + (unsigned int)(RegData)); 
	if (checksum >= 256){
		checksum = checksum - 256;
	} 
	
	packet[3]=(unsigned char)(checksum);
	
	// Set default exit value
	RxStatus = NO_ERROR;
	Escape = false;
	
	while (!Escape)
	{
	
		// Clear the input buffer
		for (i=0; i<3; i++)
		{
			IntBuffer[i]=0;
		}
	
		// Reset message length counter
		IntCharCount = 0;
	
		// Reset INTernal buffer position pointer
		IntBufferPos=0;
		
		// Reset InBound message condition
		InBoundMessage = FALSE;
		
		// Transmit the message
		for (i=0; i<4;i++)
		{
			TXREG=packet[i]; 
			while (!TXSTAbits.TRMT);
		}
	
		// Set time out timer
		TimeOut_Timer = MD_OUTBOUND_TIMEOUT;
		// Reset Rx timeout condition
		InBoundMessage_TimeOut = false;
		// Wait for a new incoming message or the timeout condition
		while (!(InBoundMessage || InBoundMessage_TimeOut));	
	
		// check if inbound message time out
		if (InBoundMessage_TimeOut ){
			
			if( tryAgain++ == MAX_RETRY )	{
				// Set timeout error condition
				RxStatus= MD_TIMEOUT_ERROR;
				// Set exit condition
				Escape = true;
			}
		}
		else if (InBoundMessage) {
		
			// Reset InBound message condition
			InBoundMessage = FALSE;
		
			// CheckSum
			PicMagDriveChecksum = (unsigned int)((unsigned int)(IntBuffer[0])+(unsigned int)(IntBuffer[1]));
			// Adjust CheckSum
			if(PicMagDriveChecksum >= 256){
				PicMagDriveChecksum = PicMagDriveChecksum - 256;
			} // end CheckSum value adjustment
			
			// Verify CheckSum
			if( PicMagDriveChecksum == (unsigned int)(IntBuffer[2])){
	
				//if ((IntBuffer[0] == 0) && (IntBuffer[1] == 0) && (IntBuffer[2]== 0)){ 
				if ((IntBuffer[0] == 0) && (IntBuffer[1] == 0) && (IntBuffer[2]== 0) && (PositionData_Zero !=0)){      
					
					if ( ++tryAgain > MAX_RETRY ){
						// Set NACK error condition
						RxStatus = MD_NACK_ERROR;
						// Set exit condition
						Escape = true;
					}
          
				}  
				else if (IntBuffer[0] == RegAdd){
					// Set no errror condition
					RxStatus = NO_ERROR; 
					// Set exit condition
					Escape = true;
				}
				else	if ( ++tryAgain > MAX_RETRY ){
						// Set NACK error condition
						RxStatus = MD_NACK_ERROR;
						// Set exit condition
						Escape = true;
				}     
			}
			else if ( ++tryAgain > MAX_RETRY )	{
					// Set check sum error condition
					RxStatus = CHECKSUM_ERROR;
					// Set exit condition
					Escape = true;
			} //  end CheckSum condition
		
		} // end InBoundMessage condition
	 
	} // end while
	
	// Switch the serial port to the EXTternal buffer
	SwitchBuffer(TRANSCEIVER);

	return RxStatus;

} // end of Send_PowerSupply_Command



/**************************************************************************
 *  
 *   getCRC16
 *  
 ***************************************************************************/
unsigned short getCRC16(unsigned char *s, unsigned char lenght)
{

 unsigned short CRCRegFull;
 unsigned short index=0;
 unsigned char x=0;
 unsigned char temp=0;
 unsigned char CheckLSB=0;

 /* load the register with 0xFFFF*/
 CRCRegFull= 0xFFFF;

 for(index=0; index<lenght; index++)
 {
  temp=s[index];

  /* XOR Low-Byte order of CRCReg and s */
  CRCRegFull^=(unsigned short)(temp);

  for (x=0; x<8; x++)
  {  
   CheckLSB=( (0x01) & (unsigned char)(CRCRegFull)) ;  
 
   /* Bit-shift once to the right */
   CRCRegFull = CRCRegFull>>1;

   /* Evaluate LSB. If it==1, XOR with 0xA001 */
   if (1 & CheckLSB)
    {
     CRCRegFull^=0xA001;
    }

  }/* Repeat for each bit in the byte */
 }/* Repeat for each character in the sequence */
 return((CRCRegFull & 0x00FF)<<8) | ((CRCRegFull & 0xFF00)>>8);
}
 

/**************************************************************************
 *  
 *  READ_EEPROM_BYTE
 *  
 ***************************************************************************/
unsigned char READ_EEPROM_BYTE(unsigned char address)
{

	// To read a data memory location, the user must write the
	// address to the EEADRL register, clear the EEPGD and
	// CFGS control bits of the EECON1 register, and then
	// set control bit RD. The data is available at the very next
	// cycle, in the EEDATL register; therefore, it can be read
	// in the next instruction. EEDATL will hold this value until
	// another read or until it is written to by the user (during
	// a write operation).

	// Data Memory Address to read
	EEADR = address;

	// Deselect Config space
	EECON1bits.CFGS = 0;

	// Point to DATA memory
	EECON1bits.EEPGD = 0;
	
	// EE Read
	EECON1bits.RD = 1;

	return EEDATA;

}



/**************************************************************************
 *  
 *  Write_EEProm_Byte
 *  
 ***************************************************************************/
void Write_EEProm_Byte(unsigned char address, unsigned char data)
{
 
	// The user must follow a specific sequence to initiate 
	// the write for each byte.

	// The write will not initiate if the following sequence is not
	// followed exactly: write 55h to EECON2, write AAh to
	// EECON2, then set WR bit, for each byte. Interrupts
	// should be disabled during this code segment.

	// Additionally, the WREN bit in EECON1 must be set to
	// enable write. This mechanism prevents accidental
	// writes to data EEPROM due to errant (unexpected)
	// code execution (i.e., lost programs). The user should
	// keep the WREN bit clear at all times, except when
	// updating EEPROM. The WREN bit is not cleared
	// by hardware.
	// After a write sequence has been initiated, clearing the
	// WREN bit will not affect this write cycle. 

	// The WR bit will be inhibited from being set unless
	// the WREN bit is set.
	// At the completion of the write cycle, the WR bit is
	// cleared in hardware and the EE Write Complete
	// Interrupt Flag bit (EEIF) is set. The user can either
	// enable this interrupt or poll this bit. EEIF must be
	// cleared by software

	// Data Memory Address to write
	EEADR = address;
	// Data Memory Value to write
	EEDATA = data;

	// Deselect Configuration space
	EECON1bits.CFGS = 0;

	// Point to DATA memory
    	EECON1bits.EEPGD = 0;
  
	// Enable writes
    	EECON1bits.WREN = 1;

	// Disable INTs
    	INTCONbits.GIE = 0;

// required sequence
//
    	EECON2 = 0x55;
   	EECON2 = 0xAA;
    	EECON1bits.WR = 1;   // start writing
//
//end required sequence

	// Disable writes
	EECON1bits.WREN = 0;

	// Wait for write to complete
	while(!EEIF)
	{
		asm("nop");
		// Disable writes
		EECON1bits.WREN = 0;
	}

	// EEIF must be cleared by software
	EEIF = 0;

	// Disable writes
    	EECON1bits.WREN = 0;

	// enable INTs
	INTCONbits.GIE = 1;

}

 /**************************************************************************
 *  
 *  SendMessage Procedure
 *  
 ***************************************************************************/
void SendMessage(unsigned char Error_Code){

	unsigned dummy_index;
	unsigned char Dummy_ByteCount;
	unsigned short CRC16;
	
	if (Error_Code){
		// Mapping outbound message
		OutModemBuffer[FUNCTION_CODE] = FUNCTION_CODE_ERROR;
		OutModemBuffer[BYTE_COUNT] = 1;
		OutModemBuffer[BYTE_COUNT +1 ] = Error_Code;
	}
	
	Dummy_ByteCount = OutModemBuffer[BYTE_COUNT] + FRAME_HEADER_LEN;
	
	// Add CRC16
	CRC16=getCRC16(OutModemBuffer,Dummy_ByteCount);
	OutModemBuffer[Dummy_ByteCount]=(CRC16 & 0xFF00)>>8;
	Dummy_ByteCount ++;
	OutModemBuffer[Dummy_ByteCount]=(CRC16 & 0x00FF);
	Dummy_ByteCount ++;
					       
	// Transmit
	for (dummy_index=0; dummy_index<Dummy_ByteCount;dummy_index++)
	{
		TXREG=OutModemBuffer[dummy_index]; 
		while (!TXSTAbits.TRMT);
	}

} // end SendMessage



 /**************************************************************************
 *  
 *  Unit_Config of the hardware profile
 *  
 ***************************************************************************/
void HW_Unit_Config(void){

	TRISA = 0x01; //RA0 Cmd_Mode enable bit
	TRISB = 0x02; //RB1 RX
	ANSELA =0x00;
	ANSELB =0x00;
	PORTA = 0x00;
	PORTB = 0x18;
		 
	//Internal Clock Source Settings...
	OSCCON=0x6A;
	
	//TIMER0 Settings....
	OPTION_REG=0xC7;
	 
	 // Interrupts Settings...
	INTCON = 0xE0;
	PIE1=0x20;
	PIE2=0x00;
	PIR1=0x00;
	PIR2=0x00;
	APFCON0=0x00;
	APFCON1=0x00;
	
	// Serial Module Settings
	TXSTA=0x20;
	RCSTA=0x90;
	SPBRG=25;
	BRG16=1;
      
} // end of Unit_HW_Config
 
  /**************************************************************************
 *  
 *  Unit_Config of the software profile
 *  
 ***************************************************************************/
void SW_Unit_Config(void){
	
	unsigned char *Dummy_P1;
	unsigned short index;
	unsigned char Dummy_Byte;
	unsigned short Comm_Error;
	
//
// READ UNIT ID

	UnitID.GroupAddress = READ_EEPROM_BYTE(EEPROM_GROUP_ADD);
	UnitID.UnitAddress = READ_EEPROM_BYTE(EEPROM_UNIT_ADD);

//
// ASSIGN DEFAULT UNIT STATUS

	UnitStatus.MaxRestartNumber = 0;
	UnitStatus.NumberRestart = 0;
	UnitStatus.Alarms = 0;
	UnitStatus.PowerSetting = 0;
	UnitStatus.PowerReading = 0;
	UnitStatus.ExpectedPower = 0;
	UnitStatus.OnLine = FALSE;
	
//
// RESET THE MASTER OFFLINE TIMERS
//
// Master On Line timeout is measured by using two nested loops. The inner loop uses
// the Master_Inner_TimeOut_Counter timer, while the outer uses the
// Master_Outer_TimeOut_Counter timer. The inner timer always uses the code fixed value
// MASTER_TIMEOUT_RELOAD, while the outer can be use values that are assigned by the user writing the
// desired value in EEPROM_MASTER_TIMEOUT. At the reset if EEPROM_MASTER_TIMEOUT is empty
// then the software will assign the default value given by MIN_MASTER_TIMEOUT

	// Set Inner timer
	Master_Inner_TimeOut_Counter = MASTER_TIMEOUT_RELOAD;
	
	// Set Outer timer
	if ((Master_OffLine_TimeOut_Reload = READ_EEPROM_BYTE(EEPROM_MASTER_TIMEOUT)) == 0xFF){
		Master_OffLine_TimeOut_Reload = MIN_MASTER_TIMEOUT;
		Write_EEProm_Byte(EEPROM_MASTER_TIMEOUT,MIN_MASTER_TIMEOUT);
	}
	Master_Outer_TimeOut_Counter = Master_OffLine_TimeOut_Reload;
	
//
// COPY THE LIGHTING SCENE TABLE

	Dummy_P1 = &SceneList.Scene_0;
	for(index=0; index < LIGHTING_SCENE_NUMBER; index++)
	{
		*(Dummy_P1) = READ_EEPROM_BYTE(EEPROM_SCENE_ADD + index);
		Dummy_P1 ++ ;
	}
	
	// Assign default value to scenes 0,1 and 2 if blank at the device reset time
	if (SceneList.Scene_0 == VALUE_EMPTY){
		SceneList.Scene_0 = SC0_DEFAULT;
		Write_EEProm_Byte(EEPROM_SCENE_ADD, SC0_DEFAULT);
	}
	
	if (SceneList.Scene_1 == VALUE_EMPTY){
		SceneList.Scene_1 = SC1_DEFAULT;
		Write_EEProm_Byte(EEPROM_SCENE_ADD+1, SC1_DEFAULT);
	}
	
	if (SceneList.Scene_2 == VALUE_EMPTY){
		SceneList.Scene_2 = SC2_DEFAULT;
		Write_EEProm_Byte(EEPROM_SCENE_ADD+2, SC2_DEFAULT);
	}
	
//
// SET RF CHANEL
//
// The value of the working channel is stored in the S2 register of the radio modem device and can
// be changed by the user. The expected value is also stored in the eeprom (EEPROM_RF_CHANNEL)
// and used as a main reference. At the reset time if EEPROM_RF_CHANNEL is not blank then
// its value is copied in the S2 regster.
	
	// Switch the serial port to the EXTernal buffer
	SwitchBuffer(TRANSCEIVER);
	
	// Assign the default RF Channel value at bootstrap
	Dummy_Byte = READ_EEPROM_BYTE(EEPROM_RF_CHANNEL);
	if(Dummy_Byte < 0x0A){
		Comm_Error = WriteModemChannel(Dummy_Byte);
	}
	
//
// SET COMMAND MODE WAITING STATUS
//
// CmdMode_TimeOut_Timer is used at the bootstrap To wait
// whether any external controller is on line on the internal buffer.
// The device can be entered in Command Mode both by software (Fucntion Code 200)
// or by hardware (lowering the RA0 input).
// CMD_MODE_TIMEOUT  is the length of time the device will wait for a valid command
//  then, if CmdMode_TimeOut_Timer > CMD_MODE_TIMEOUT  it will return to its normal
// operating conditions.
// At any time, after the CMD_MODE_TIMEOUT time, the device can
// still be set in command mode by lowering the line RA0.
	
	// Switch the serial port to the INTernal buffer
	SwitchBuffer(CMD_MODE);
	
	// Reset command mode flag
	Cmd_Mode_Sw = Cmd_Mode_Hw  = Cmd_Mode = FALSE;
	
	CmdMode_TimeOut_Timer = 0;
	CmdMode_TimeOut = FALSE;
	
	Bootstrap_TimeOut_Timer = 0;
	Bootstrap_TimeOut = FALSE;

//
// GENRAL SETTINGS

	// Clear the internal buffer
	for (index=0; index<INT_BUFFER_DIM;index++) {IntBuffer[index]=0;}
	
	// Set default online master status = False
	Master_OnLine = FALSE;
	Release_Alvara = FALSE;
	
	// Set the initial status: IDLE
	SystemStatus = IDLE;
	
	// General
	RxStatus = 0;
	RxModemTimeOut = 0;
	InBoundMessage_TimeOut = 0;
	TimeOut_Timer = 0;
	ScanMagDrive = FALSE;
	FrameLength = 0;
	RxIdleCheck = 0;
		
} // end of Unit_SW_Config


 /**************************************************************************
 *  
 *  Execute FC
 *  
 *	Execute the fucntion code given as Fucntion_Code and return:
 *  
 *	-  NO_ERROR
 *	- CHECKSUM_ERROR
 *	- MD_NACK_ERROR
 *	- MD_TIMEOUT_ERROR
 *	- FC_NACK
 *	- FC_CAST_ERROR
 *	- RM_NACK_ERROR
 *	- RM_TIMEOUT_ERROR
 *
 *	Inbound_Message is used as a flag for skipping execution of
 *	codes not allowed if the messages are broadcast/multicast
 *	When no errors occur OutModemBuffer will contain the message
 *	to be send back to the master.
 ***************************************************************************/
unsigned char Execute_FC(unsigned char Function_Code, unsigned char Dummy_ByteCount, unsigned char Inbound_Broadcast ){
	
	unsigned short index;
	unsigned char Dummy_DataPos;
	unsigned char Dummy_Byte;
	unsigned char Comm_Error;
	unsigned char *Dummy_P1;
	unsigned char *Dummy_P2;
	
	unsigned char FC_Comm_Error;
		
				
	if ((Function_Code == READ_RAM) || (Function_Code == READ_EEPROM)){
		
		//
		// READ_RAM or READ_EEPROM - FC 0x00 (0) or 0x40 (64)
		//		
			
		// Exit if the message is received while in Command Mode and after having sent out an error message
		if(Cmd_Mode){
  			FC_Comm_Error = FC_NACK;
		}
		else{

			// Number of registers to be read
			DataLength = ExtBuffer[BYTE_COUNT];
							
			// Mapping outbound message

			// Outbound data byte count
			OutModemBuffer[Dummy_ByteCount] = DataLength;
			Dummy_ByteCount ++ ;

			// Fill up Data Field			
			Dummy_DataPos = 0;
			FC_Comm_Error = NO_ERROR;
			while ((Dummy_DataPos < DataLength) && !(FC_Comm_Error)){ 
				
				FC_Comm_Error = Send_PowerSupply_Command(ExtBuffer[FUNCTION_CODE],ExtBuffer[FRAME_HEADER_LEN+Dummy_DataPos],0);
				Dummy_DataPos ++ ;
				
				if(!(FC_Comm_Error)){
					OutModemBuffer[Dummy_ByteCount] = IntBuffer[1]; 
					Dummy_ByteCount ++;
				}
				else if (FC_Comm_Error == MD_TIMEOUT_ERROR){
					 UnitStatus.OnLine = false;
				}	 

			} // end of While Loop

		}  // end if (!(Inbound_Broadcast))
		
	} // End Function Code: READ_RAM or READ_EEPROM
	else if ( Function_Code == READ_SLAVE_STATUS){
		
		//
		// READ_SLAVE_STATUS - FC: 0x05 (5)
		//
	
		// Exit if the message is received while in Command Mode and after having sent out an error message
		if(Cmd_Mode){
  			FC_Comm_Error = FC_NACK;
		}
		else{
			
			// up date power supply status
			GetUnitStatus(DATA_START);
			
			// Outbound data byte count
			OutModemBuffer[BYTE_COUNT] = PS_STATUS_LEN +LIGHTING_SCENE_NUMBER;

		  	FC_Comm_Error = NO_ERROR;
		  	
		}// end of else  	
		
	}//  End Function Code: READ_SLAVE_STATUS
	else if (Function_Code ==  READ_RADIO_MODEM_CHANNEL){
		
		//
		// READ_RADIO_MODEM_CHANNEL - FC: 0x06(6)
		//
		SwitchBuffer(TRANSCEIVER);

		// Mapping outgoing message
		//  outgoing data byte count
		OutModemBuffer[Dummy_ByteCount ] = 1;
		Dummy_ByteCount ++;
		FC_Comm_Error = ReadModemChannel(&OutModemBuffer[Dummy_ByteCount]);
			
		if(Cmd_Mode){SwitchBuffer(CMD_MODE);}
		
	} // End Function Code: READ_RADIO_MODEM_CHANNEL
	else if (Function_Code == GET_DEVICE_ID){
		
		//
		// GET_DEVICE_ID - FC: 0x0A (10)
		//

		// Mapping outbound message
			
		//  Outbound data byte count
		DataLength = READ_EEPROM_BYTE(EEPROM_DEVICE_ID);

		// exit when the empty value(255) is read
		if (DataLength == VALUE_EMPTY){DataLength=0;}

		OutModemBuffer[BYTE_COUNT] = DataLength;
		Dummy_ByteCount ++;

		EEProm_Address = EEPROM_DEVICE_ID + 1;					
		// Data Field
		for (index=0;index<DataLength; index ++)
		{
			OutModemBuffer[Dummy_ByteCount] = READ_EEPROM_BYTE(EEProm_Address);
			Dummy_ByteCount ++;
			EEProm_Address ++;
		}

	  	FC_Comm_Error = NO_ERROR;
		
	} // End Function Code: GET_DEVICE_ID
	else if (Function_Code == GET_FW_ID){
		
		//
		// GET_FW_ID - FC: 0x0B (11)
		//
	
		// Mapping outbound message

		OutModemBuffer[BYTE_COUNT] = 0;
		Dummy_ByteCount ++;

		// Data Field
		Dummy_P1 = (unsigned char *) FirmwareID;
		while (*(Dummy_P1) != '\0') 
		{ 
			OutModemBuffer[Dummy_ByteCount] = *(Dummy_P1);
			Dummy_ByteCount ++;
			Dummy_P1 ++ ;
		} 
		
		OutModemBuffer[Dummy_ByteCount] = ALVARA_GENDER;
		Dummy_ByteCount ++;
			
		//  outbound data byte count
		OutModemBuffer[BYTE_COUNT] = Dummy_ByteCount -  FRAME_HEADER_LEN;

	  	FC_Comm_Error = NO_ERROR;
		  	
	} // End Function Code: GET_FW_ID
	else if (Function_Code == GET_SCENE){
		
		//
		// GET SCENE - FC: 0x0D (13)
		//
									
		// Mapping outbound message
					
		// Outbound data byte count
		OutModemBuffer[Dummy_ByteCount] = ExtBuffer[BYTE_COUNT];
		Dummy_ByteCount ++;
		
		// Data Field
		FC_Comm_Error = FALSE;
		for (index=0;index<ExtBuffer[BYTE_COUNT]; index ++)
		{
			EEProm_Address = EEPROM_SCENE_ADD + ExtBuffer[FRAME_HEADER_LEN + index];
			OutModemBuffer[Dummy_ByteCount] = READ_EEPROM_BYTE(EEProm_Address);

			// Check for Invalid Cast Exception: scene address overflow
			if (EEProm_Address > (EEPROM_SCENE_ADD + LIGHTING_SCENE_NUMBER - 1))
			{
				FC_Comm_Error = FC_CAST_ERROR;
			}
			else
			{
				Dummy_P1 = &SceneList.Scene_0 + ExtBuffer[FRAME_HEADER_LEN + index];
				*(Dummy_P1) = OutModemBuffer[Dummy_ByteCount];
			}

			Dummy_ByteCount ++;
			
		} // end for index
			
	} // End Function Code: GET_SCENE
	else if ((Function_Code == GET_MASTER_TIMEOUT) || (Function_Code == READ_RADIO_MODEM_CHANNEL_EEPROM)){
		
		//
		// GET_MASTER_TIMEOUT - FC: 0x0E (14)
		//

		// Mapping outbound message
				
		// data pointer adjustment
		Dummy_ByteCount ++;
		
		// Data Field
		EEProm_Address = EEPROM_MASTER_TIMEOUT;
		if (Function_Code == READ_RADIO_MODEM_CHANNEL_EEPROM){
			EEProm_Address = EEPROM_RF_CHANNEL;
		}	
		OutModemBuffer[Dummy_ByteCount] = READ_EEPROM_BYTE(EEProm_Address);
		Dummy_ByteCount ++;

		//  outbound data byte count
		OutModemBuffer[BYTE_COUNT]= Dummy_ByteCount - FRAME_HEADER_LEN;

	  	//SendMessage(NO_ERROR);
	  	FC_Comm_Error = NO_ERROR;
		
	} // End Function Code: GET_MASTER_TIMEOUT
	else if (Function_Code == READ_ALL_EEPROM){
		
		//
		// READ_ALL_EEPROM - FC: 0x3F (63)
		//

		// Exit if the message is received while in Command Mode and after having sent out an error message
		if(Cmd_Mode){
  			FC_Comm_Error = FC_NACK;
		}
		else{
			
			// Mapping outgoing message
			//  outgoing data byte count
			OutModemBuffer[Dummy_ByteCount] = PS_EEPROM_LEN;
			Dummy_ByteCount ++;


			// Data Field			
			Dummy_DataPos = 0;
			FC_Comm_Error = NO_ERROR;
			while ((Dummy_DataPos != PS_EEPROM_LEN) && !(FC_Comm_Error))
			{ 			
				FC_Comm_Error = Send_PowerSupply_Command(READ_EEPROM,Dummy_DataPos,0);
				Dummy_DataPos ++ ;
				if (!(FC_Comm_Error)){
					OutModemBuffer[Dummy_ByteCount] = IntBuffer[1]; 
					Dummy_ByteCount ++;
				}
				else if (FC_Comm_Error == MD_TIMEOUT_ERROR){
					UnitStatus.OnLine = false;
				}
					
			} // end of while
			
		}// end else
			
	} // End Function Code: READ_ALL_EEPROM
	else if ((Function_Code == WRITE_RAM) || (Function_Code == WRITE_EEPROM)){
		
		//
		// WRITE_RAM or WRITE_EEPROM - FC: 0x80 (128) or 0xC0 (192)
		//
			
		// Exit if the message is received while in Command Mode and after having sent out an error message
		if(Cmd_Mode){
  			FC_Comm_Error = FC_NACK;
		}
		else{
			
			// Mapping outgoing message
		
			//  Outgoing data byte count (ExtBuffer[BYTE_COUNT] / 2)
			DataLength = ExtBuffer[BYTE_COUNT];
			OutModemBuffer[Dummy_ByteCount] = DataLength >> 1;
			Dummy_ByteCount ++;

			// Data Field
			Dummy_DataPos =  Dummy_ByteCount;
			FC_Comm_Error = NO_ERROR;
			
			while ((Dummy_DataPos < (DataLength + FRAME_HEADER_LEN)) && !(FC_Comm_Error)){ 
				
				FC_Comm_Error = Send_PowerSupply_Command(ExtBuffer[FUNCTION_CODE],ExtBuffer[Dummy_DataPos],ExtBuffer[Dummy_DataPos+1]);
				Dummy_DataPos +=2 ;
	
				if (!(FC_Comm_Error)){
					
					OutModemBuffer[Dummy_ByteCount] = IntBuffer[1];
					Dummy_ByteCount ++ ;
				
					// update unit status element
					if(ExtBuffer[FUNCTION_CODE] == WRITE_EEPROM)	{
						
						switch (ExtBuffer[Dummy_ByteCount-1]){
											
							case MAX_NUMBER_RESTART:
								UnitStatus.MaxRestartNumber=IntBuffer[1];
								break;
							
							case NUMBER_OF_RESTART:
								UnitStatus.NumberRestart=IntBuffer[1];
								break;
							
							case ALARMS:
								UnitStatus.Alarms=IntBuffer[1];
								break;
													
							default: break;
			
						}// end switch(ExtBuffer[Dummy_ByteCount-1])
			
					} // end if WRITE_EEPROM
	
					// update unit status element
					if(ExtBuffer[FUNCTION_CODE] == WRITE_RAM){
						
						switch (ExtBuffer[Dummy_ByteCount-1])	{
			
							case POWER_SETTING:
								UnitStatus.PowerSetting = IntBuffer[1];
								UnitStatus.ExpectedPower = IntBuffer[1];
								break;
													
							default: break;
							
						} // end switch (ExtBuffer[Dummy_ByteCount-1])
						
					} // end if WRITE_RAM	
			
				} // end NO_ERROR
				else if (FC_Comm_Error == MD_TIMEOUT_ERROR){
					
					UnitStatus.OnLine = false;
				}

			} // end while loop
		
		} // end if(Cmd_Mode)
	  	
	} // End Function Code: WRITE_RAM or WRITE_EEPROM
	else if ((Function_Code == SET_RADIO_MODEM_CHANNEL) && (!(Inbound_Broadcast))){
		
		//
		// SET_RADIO_MODEM_CHANNEL - FC: 0x86(134)
		//
		
		SwitchBuffer(TRANSCEIVER);
		
		// Mapping outbound message
		
		// Outbound data byte count
		OutModemBuffer[Dummy_ByteCount] = 1;
		Dummy_ByteCount ++;
		OutModemBuffer[Dummy_ByteCount] = ExtBuffer[DATA_START];
		
		FC_Comm_Error = WriteModemChannel(ExtBuffer[DATA_START]);
		if (!(FC_Comm_Error)){
			// Save data in EEPROM
			Write_EEProm_Byte(EEPROM_RF_CHANNEL,ExtBuffer[DATA_START]);
		}

		if(Cmd_Mode){
			SwitchBuffer(CMD_MODE);
		}	
			
	}  // End Function Code: READ_RADIO_MODEM_CHANNEL
 	else if (Function_Code == GOTO_SCENE){
	 	
	 	//
		// GOTO_SCENE - FC: 0x88 (136)
		//
		
		// Exit if the message is received while in Command Mode and after having sent out an error message
		if(Cmd_Mode){
    			FC_Comm_Error = FC_NACK;
		}
		else{
			
			// Mapping outbound message
			
			// OutBound
			OutModemBuffer[Dummy_ByteCount] = ExtBuffer[BYTE_COUNT];
			Dummy_ByteCount ++;
	
			// Read new power level
	               	EEProm_Value = READ_EEPROM_BYTE(EEPROM_SCENE_ADD + ExtBuffer[DATA_START]);
	
			// Check if the vaue is correct
			if (EEProm_Value == VALUE_EMPTY) {
				FC_Comm_Error = FC_CAST_ERROR;
			}
			else	{
				
				// Set new power level
				FC_Comm_Error = Send_PowerSupply_Command(WRITE_RAM,POWER_SETTING,EEProm_Value);
				if (!(FC_Comm_Error))	{
	
					// update the unit's status array
					UnitStatus.PowerSetting=IntBuffer[1];
					UnitStatus.ExpectedPower = IntBuffer[1];
					// write the answer into the output buffer
					OutModemBuffer[Dummy_ByteCount]=IntBuffer[1];
											
				}// end if Send_PowerSupply_Commnand
	
			}// end if(EEProm_Value = VALUE_EMPTY)
	
		} // end if(Cmd_Mode)

	} // End Function Code: GOTO_SCENE
	else if ((Function_Code == SET_DEVICE_ID) && (!(Inbound_Broadcast))){
		
		//
		// SET_DEVICE_ID - FC: 0x8A (138)
		//

		// Mapping outbound message
							
		//  outbound data byte count
		OutModemBuffer[Dummy_ByteCount] = ExtBuffer[BYTE_COUNT];
		Dummy_ByteCount ++;

		if (ExtBuffer[BYTE_COUNT] > MAX_ID_LEN){
			FC_Comm_Error = FC_CAST_ERROR;
		}
		else{
			
			// eeprom modify/read
			Write_EEProm_Byte((EEPROM_DEVICE_ID),ExtBuffer[BYTE_COUNT]);
	
			// Data Field
			Dummy_P1 = &ExtBuffer[DATA_START];
			EEProm_Address = EEPROM_DEVICE_ID + 1;
			for (index=0;index<ExtBuffer[BYTE_COUNT]; index ++){
				// eeprom modify/read
				Write_EEProm_Byte((EEProm_Address + index),*(Dummy_P1));
				OutModemBuffer[Dummy_ByteCount] = READ_EEPROM_BYTE(EEProm_Address + index);
				Dummy_ByteCount ++;
				Dummy_P1 ++;
			}
	
			FC_Comm_Error = NO_ERROR;
			
		} // end if (ExtBuffer[BYTE_COUNT] > MAX_ID_LEN)
			
	} // End Function Code: SET_DEVICE_ID
	else if ((Function_Code == SET_UNIT_ID) && (!(Inbound_Broadcast))){
		
		//
		// SET_UNIT_ID - FC: 0x8C (140)
		//
		
		// Exit if either the new GroupAddress or the new UnitAddress is equal to zero: not accepted
		if ((ExtBuffer[DATA_START] == 0) || (ExtBuffer[DATA_START+1]) == 0){
			FC_Comm_Error = FC_CAST_ERROR;
		}
		else{
		
			// Mapping outbound message
			
			//  outbound data byte count					
			OutModemBuffer[Dummy_ByteCount]=ExtBuffer[BYTE_COUNT];
			Dummy_ByteCount ++;
			
			// Data Field
			Dummy_P1 = &ExtBuffer[DATA_START];
			Dummy_P2 = &UnitID.GroupAddress;
			for (index=0;index<ExtBuffer[BYTE_COUNT]; index ++)
			{
				Write_EEProm_Byte((EEPROM_GROUP_ADD + index),*(Dummy_P1));
				*(Dummy_P2) = READ_EEPROM_BYTE(EEPROM_GROUP_ADD + index);
				OutModemBuffer[Dummy_ByteCount] = *(Dummy_P2);
				Dummy_ByteCount ++;
				Dummy_P1 ++;
				Dummy_P2 ++;
			}

		  	FC_Comm_Error = NO_ERROR;
		  	
		} // end (!(ExtBuffer[DATA_START] & ExtBuffer[DATA_START+1]))

	} // End Function Code: SET_UNIT_ID
	else if (Function_Code == SET_SCENE ){
		
		//
		// SET SCENE - FC: 0x8D (141)
		//
	
		// Mapping outgoing message
		
		//  Inbound data byte count
		DataLength = ExtBuffer[BYTE_COUNT];

		// Outbound data byte count
		OutModemBuffer[Dummy_ByteCount] = DataLength >> 1;
		Dummy_ByteCount ++;
		
		FC_Comm_Error = NO_ERROR;
		for (index= Dummy_ByteCount; index < (DataLength + FRAME_HEADER_LEN); index += 2){
		 
			EEProm_Value= ExtBuffer[index+1];
			EEProm_Address = EEPROM_SCENE_ADD + ExtBuffer[index];

			Dummy_P1 = &SceneList.Scene_0 + ExtBuffer[index];

			// Check for Invalid Cast Exception: invalid scene value or scene address overflow
			if (EEProm_Address > (EEPROM_SCENE_ADD + LIGHTING_SCENE_NUMBER - 1))	{
			 	FC_Comm_Error = FC_CAST_ERROR;
			}
			else if (( EEProm_Value > HIGHER_HIGH_POWER_LEVEL) && ( EEProm_Value < VALUE_EMPTY)){
				FC_Comm_Error = FC_CAST_ERROR;
			}
			else{
				 // Set the new scene's value only when it is in the right interval:
				Write_EEProm_Byte(EEProm_Address, EEProm_Value);
				*(Dummy_P1) = EEProm_Value;
			}

			OutModemBuffer[Dummy_ByteCount] = READ_EEPROM_BYTE(EEProm_Address);
			Dummy_ByteCount ++ ;
		}

 	} // End Function Code: SET_SCENE
 	else if ((Function_Code == SET_MASTER_TIMEOUT) || (Function_Code == SET_RADIO_MODEM_CHANNEL_EEPROM)){
	 	
	 	//
		// SET_MASTER_TIMEOUT - FC: 0x8E (142)
		//
		// SET_RADIO_MODEM_CHANNEL_EEPROM - FC: 0x87 (135)
		//
	
		// Mapping outbound message
		
		//  outbound data byte count
		OutModemBuffer[Dummy_ByteCount] = 1;
		Dummy_ByteCount ++;
		
		EEProm_Value = ExtBuffer[DATA_START];
		
		FC_Comm_Error = NO_ERROR;
		
		if (Function_Code == SET_MASTER_TIMEOUT){
			
			// check time out boundaries
			if ( EEProm_Value > MAX_MASTER_TIMEOUT ){
				EEProm_Value = MAX_MASTER_TIMEOUT;
			}
			else if ( EEProm_Value < MIN_MASTER_TIMEOUT ){
				EEProm_Value = MIN_MASTER_TIMEOUT;
			}
	
			EEProm_Address =  EEPROM_MASTER_TIMEOUT;
			Write_EEProm_Byte(EEProm_Address, EEProm_Value);
	
			Master_OffLine_TimeOut_Reload = READ_EEPROM_BYTE(EEProm_Address);
			OutModemBuffer[Dummy_ByteCount] = Master_OffLine_TimeOut_Reload;
	
			// Reset master off-line timer
			Master_Inner_TimeOut_Counter = MASTER_TIMEOUT_RELOAD;
			Master_Outer_TimeOut_Counter = Master_OffLine_TimeOut_Reload;
		}	
		else{
			
			// check time out boundaries
			FC_Comm_Error = FC_CAST_ERROR;
			if ( EEProm_Value < 0x0A){
				EEProm_Address =  EEPROM_RF_CHANNEL;
				Write_EEProm_Byte(EEProm_Address, EEProm_Value);
				OutModemBuffer[Dummy_ByteCount] = READ_EEPROM_BYTE(EEProm_Address);
				FC_Comm_Error = NO_ERROR;
			}
		}	
  	} // End Function Code: SET_MASTER_TIMEOUT
  	else{
	  	
		FC_Comm_Error = FC_NACK;
		
	}// end of switch
	
	return FC_Comm_Error;

}// end of Execute_Write_FC

////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//	MAIN PROGRAM
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void main() { 

unsigned short i;

unsigned char Dummy_ByteCount;
unsigned char Dummy_DataPos;
unsigned char Dummy_Byte;

unsigned char Comm_Error;

unsigned char *Dummy_P1;
unsigned char *Dummy_P2;


//
// Device HW CONFIG
HW_Unit_Config();

//
// Device SW CONFIG
SW_Unit_Config();

// Big Loop
while(1)
{  

// RESET WATCHDOG TIMER
	CLRWDT();

// CHECK IF COMMAND MODE

	Cmd_Mode_Hw = (!(RA0));
	Cmd_Mode = Cmd_Mode_Sw | Cmd_Mode_Hw;
	
	if (Cmd_Mode){
		if (BufferMode == TRANSCEIVER){
			SwitchBuffer(CMD_MODE);
		}
	}
	else if ((Bootstrap_TimeOut) && (BufferMode != TRANSCEIVER)){
		SwitchBuffer(TRANSCEIVER);
	}	
	
// STATUS MACHINE

	switch(SystemStatus) 
  	{

// IDLE STATE
//
// The system remains in the Idle state as long as:
// 1.  A new message from the master controller is received
// 2. The timer ticks to keep the MagDrive in command mode: ScanMagDrive = TRUE
// 3. The Command Mode is force by software/hardware: Cmd_Mode = TRUE
//

		case IDLE:

		     	if (InBoundModemMessage){

// INBOUND MESSAGE			     	
// Check if a new valid message from the master controller has been received

				// Reset InBoundModemMessage flag
				InBoundModemMessage = FALSE;

				// Validate inbound message
				Comm_Error = Verify_Incoming_Message(&ExtBuffer,FrameLength);
				if (!(Comm_Error)){
					
				// (CRC: ACK; Group Address: ACK; Unit Address: ACK)
				// unicast, multicast or broadcast message

					SystemStatus = COMMAND_RECEIVED;
	
					// Reset CmdMode_TimeOut_Timer
					if (Cmd_Mode_Sw){						
						CmdMode_TimeOut_Timer = 0;
						Cmd_Mode_Sw = TRUE;
					}
							
				} // end case (CRC: ACK; Group Address: ACK; Unit Address: ACK)
					
		      } // end if InBoundModemMessage
		      else if((ScanMagDrive)&&(!(BufferMode == CMD_MODE))){

// MAGDRIVE BEACON
// Check if the timer has raised the scan_magdrive condition

		        	ScanMagDrive = FALSE;
				// Set the new system status @ POLL_MAGDRIVE
		        	SystemStatus = POLL_MAGDRIVE;
		        }
		       else if((Release_Alvara)&&(!(BufferMode == CMD_MODE))){

// MASTER TIME OUT
// Check if the master communication went in time out
		      	
				// reset the release condition
				Release_Alvara = FALSE;

				// Set the new system status @ RELEASE_MAGDRIVE
		        	SystemStatus = RELEASE_MAGDRIVE;
		        }

		       break;   // End of IDLE

// COMMAND_RECEIVED STATE
//
// the system moves from the IDLE state into the COMMAND_RECEIVED state
// as soon as a new message from the master controller has been stored in
// the receiver buffer
//

		case COMMAND_RECEIVED:
		
			// set the new system status @ IDLE
			SystemStatus=IDLE;

			// Write the header of the outgoing message:
			// Group Address + Unit Address + Function Code

			Dummy_ByteCount = 0;

			//  Group Addres + Unit Address
			OutModemBuffer[Dummy_ByteCount] = UnitID.GroupAddress;
			Dummy_ByteCount ++;
			OutModemBuffer[Dummy_ByteCount] = UnitID.UnitAddress;
			Dummy_ByteCount ++;
					
			// Function Code
			OutModemBuffer[Dummy_ByteCount] = ExtBuffer[FUNCTION_CODE];
			Dummy_ByteCount ++;

			if ((ExtBuffer[FUNCTION_CODE] == SET_COMMAND_MODE) && (BufferMode == CMD_MODE)){
				
				// Set_Command_Mode can be received only when wired connected to a controller
				// Set the command mode flag to true and reset the timer
				CmdMode_TimeOut_Timer = 0;
				Cmd_Mode_Sw = TRUE;

				// Mapping outbound message:

				// Outbound data byte count
				OutModemBuffer[Dummy_ByteCount] = 0;
				Dummy_ByteCount ++;

				SendMessage(NO_ERROR);
				
			} // end case Function Code: SET_COMMAND_MODE
			else if ((ExtBuffer[FUNCTION_CODE] == EXIT_COMMAND_MODE) && (BufferMode == CMD_MODE)){
			
				// Exit_Command_Mode can be received only when wired connected to a controller
				// Mapping outbound message:

				// Outbound data byte count
				OutModemBuffer[Dummy_ByteCount] = 0;
				Dummy_ByteCount ++;

				SendMessage(NO_ERROR);

				SwitchBuffer(TRANSCEIVER);

				// Set the command mode flag to false					
				Cmd_Mode_Sw = FALSE;
										
			} // end case Function Code: EXIT COMMAND MODE
			else if ((ExtBuffer[FUNCTION_CODE] == RESET_DEVICE)){// && (BufferMode == CMD_MODE)){
				
				// Reset_Command_Mode can be received only when wired connected to a controller
				// Mapping outbound message:

				// Outbound data byte count
				OutModemBuffer[Dummy_ByteCount] = 0;
				Dummy_ByteCount ++;

				SendMessage(NO_ERROR);

				// RESET
				RESET();
								
			} // end case Function Code: RESET_DEVICE
			else {
				
				Broadcasted_Inbound_Message = FALSE;		
				if((ExtBuffer[GROUP_ADDRESS]==BROADCAST) || (ExtBuffer[UNIT_ADDRESS]==MULTICAST)){
					Broadcasted_Inbound_Message = TRUE;
				}	
				Comm_Error = Execute_FC(ExtBuffer[FUNCTION_CODE], Dummy_ByteCount, Broadcasted_Inbound_Message);
				if (!(Broadcasted_Inbound_Message)){
					SendMessage(Comm_Error);
				}
			}
			
			break;   // End of COMMAND_RECEIVED

//          
// POLL_MAGDRIVE STATE
//

		case POLL_MAGDRIVE:
		        
			// set the new system status @ IDLE
			SystemStatus=IDLE;
			Send_PowerSupply_Command(READ_RAM,ELECTRICAL_POWER,0);

			break;  // End of  POLL_MAGDRIVE STATE

//          
// RELEASE_MAGDRIVE
//
		case RELEASE_MAGDRIVE:
	
			// set the new system status @ IDLE
			SystemStatus=IDLE;

//			// set the power level to the default value written in the eeprom of the power supply
//			Send_PowerSupply_Command(WRITE_RAM,POWER_SETTING,MAX_POWER);
//
//			// read the last status before release the MD
			GetUnitStatus(0x00);

			break;  // End of  RELEASE_MAGDRIVE
				
//
// DEFAULT
//
		default:

        		// set the new system status @ IDLE
			SystemStatus=IDLE;

			break;  // End of default

	}// END SystemStatus

}//END Big Loop

}//END Main Program